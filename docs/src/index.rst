====================================
TMC MCCS Leaf Nodes documentation
====================================

This project is developing the MCCS Leaf Nodes component of the Telescope Monitoring and Control (TMC) prototype, for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/


.. toctree::
   :maxdepth: 1
   :caption: MCCS Master Leaf Node

   mccsmasterleafnode/ska_tmc_mccsmasterleafnode

.. toctree::
   :maxdepth: 1
   :caption: MCCS Master component_manager

   mccsmasterleafnode/ska_tmc_mccsmasterleafnode_manager

.. toctree::
   :maxdepth: 1
   :caption: MCCS Master Leaf Node Commands

   mccsmasterleafnode/ska_tmc_mccsmasterleafnode_commands


.. toctree::
   :maxdepth: 1
   :caption: MCCS Subarray Leaf Node

   mccssubarrayleafnode/ska_tmc_mccssubarrayleafnode


.. toctree::
   :maxdepth: 1
   :caption: MCCS Subarray component_manager

   mccssubarrayleafnode/ska_tmc_mccssubarrayleafnode_manager

.. toctree::
   :maxdepth: 1
   :caption: MCCS Subarrary Leaf Node commands

   mccssubarrayleafnode/ska_tmc_mccssubarrayleafnode_commands



Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

* :ref:`search`
