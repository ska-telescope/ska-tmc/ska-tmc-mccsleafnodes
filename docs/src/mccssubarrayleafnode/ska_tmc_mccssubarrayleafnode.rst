ska\_tmc\_mccssubarrayleafnode package
======================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4


Submodules
----------

ska\_tmc\_mccssubarrayleafnode.mccs\_subarray\_leaf\_node module
----------------------------------------------------------------

.. automodule:: ska_tmc_mccssubarrayleafnode.mccs_subarray_leaf_node
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: ska_tmc_mccssubarrayleafnode
   :members:
   :undoc-members:
   :show-inheritance:



#####################################
Properties in MCCS Subarray Leaf Node
#####################################

+-------------------------------+---------------+--------------------------------------------------------------------------------+
| Property Name                 | Data Type     | Description                                                                    |
+===============================+===============+================================================================================+
| MccsMasterFQDN                | DevString     | FQDN of the MCCS Master Tango Device Server.                                   |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| MccsSubarrayFQDN              | DevString     | FQDN of the MCCS Subarray Tango Device Server.                                 |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| LivelinessCheckPeriod         | DevFloat      | Period for the liveliness probe to monitor each device in a loop.              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| EventSubscriptionCheckPeriod  | DevFloat      | Period for the event subscriber to check the device subscriptions in a loop.   |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| AdapterTimeOut                | DevFloat      | Timeout for the adapter creation. This property is for internal use.           |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| CommandTimeOut                | DevFloat      | Timeout for the command execution                                              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+