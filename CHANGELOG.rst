###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[0.1.5]
************
* Updated Name of the variable sent to update_task_status


[0.2.0]
************
* This release fixes the bug for Restart command documented under HM-371 on MCCS SLN device
* The fix applied is - extracting the subarray id from the device FQDN and using it as input for Restart command


[0.3.0]
************

* MCCS SLN Component Manager is updated to handle longRunningCommandResult events from MCCS Subarray device in accordance to TMC-MCCS pairwise testing.
* MCCS MLN Component Manager has now updated event handling for longRunningCommandResult events.
* ReleaseAllResources command no longer supports timeout propagation, as it is a FastCommand on MCCS Controller.
* Event Receiver is fixed to subscribe events only once per device per attribute.

[0.4.0]
************

* Upgraded MCCSMasterLeaf Node and MCCSSubarrayLeaf Node to make it compliant with base class version 1.0.0
* Upgraded MCCSMasterLeaf Node and MCCSSubarrayLeaf Node to  make it compliant with pytango version 9.5.0
* After queuing methods added for long running commands.


[0.4.1]
******

* Improve logger statments and add timeout and error propagation decorators for AssignResources and Configure command
* Utilise latest ska-tmc-common package

[0.4.2]
******
* Set and push archive events for all the attributes

[0.4.3]
*******

* Fix Timer Thread Behavior on Scan EndScan and Improve Command Timeout Handling

[0.5.0]
*******
* Accomodate changes of liveliness probe

[0.5.1]
*******
* Update ska-tmc-common v0.22.4 that includes fix for SKB-627

[0.5.2]
*******
* Fix bug skb-525

[0.5.3]
*******
* Resolve bug skb-658

[0.6.0]
*******
* Update the adminMode functionality

Fixed
-----
* Fixed Pylint warnigs

[0.5.3]
*******
* Resolved issue related to Abort

[0.6.1]
*******
* Resolved issue related to Scan

[0.6.2]
*******
* Updated the FQDN according to ADR-9 compliance
