"""EndScan command class for LowTmcLeafNodeMccsSubarray."""

import time
from logging import Logger
from typing import Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class EndScan(MccsSLNCommand):
    """
    This class implements the EndScan command for MCCS Subarray Leaf Node
    device.

    It provides methods to invoke endscan command on the Mccs Subarray device
    and  handle its execution.
    """

    def __init__(self, component_manager, logger: Logger) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger=self.logger
        )

    @timeout_tracker
    @error_propagation_tracker("get_obs_state", [ObsState.READY])
    def endscan(
        self,
    ) -> None:
        """
        This method invokes the do hook for the EndScan command class.
        It also sets up the command tracking.
        """
        return self.do()

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        try:
            result = kwargs.get("result")
            status = kwargs.get("status", TaskStatus.COMPLETED)
            message = kwargs.get(
                "exception", "Received exception in processing"
            )
            self.logger.info("Updating task status with kwargs: %s", kwargs)

            if status == TaskStatus.ABORTED:
                self.task_callback(
                    result=(ResultCode.ABORTED, "Command has been aborted"),
                    status=status,
                )
                return
            if result[0] == ResultCode.OK:
                self.component_manager.command_in_progress = None
                self.task_callback(result=result, status=status)

            else:
                self.task_callback(
                    result=result,
                    status=status,
                    exception=message,
                )
                self.component_manager.command_in_progress = None

            self.component_manager.command_id = ""
        except (KeyError, ValueError) as exception:
            self.logger.exception(
                "Exception occured while updating task status %s"
                "Received task_status: %s",
                exception,
                kwargs,
            )
        self.logger.info("EndScan command task status updated to: %s", kwargs)

    # pylint: disable=arguments-differ
    def do(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on Mccs Subarray.

        :return: Tuple[ResultCode, str]
        """

        # Stopping tracker thread of Scan Command
        self.component_manager.stop_scan_tracking()

        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Subarray",
            self.mccs_subarray_adapter,
            "EndScan",
        )
        self.logger.info(
            "%s EndScan command -> ResultCode: %s and Message: %s",
            self.mccs_subarray_adapter.dev_name,
            result_code,
            unique_id_or_message,
        )
        return result_code[0], unique_id_or_message[0]
