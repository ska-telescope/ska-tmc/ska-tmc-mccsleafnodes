"""
Configure command class for LowTmcLeafNodeMccsSubarray.
"""

import time
from logging import Logger
from typing import Callable, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class Configure(MccsSLNCommand):
    """
    This class implements the Configure command for MCCS Subarray Leaf Node
    device.

    It provides methods to configure the Mccs Subarray device and
    handle the execution of the Configure command.
    """

    def __init__(self, component_manager, logger: Logger) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.task_callback: TaskCallbackType

    @timeout_tracker
    @error_propagation_tracker(
        "get_obs_state", [ObsState.CONFIGURING, ObsState.READY]
    )
    def invoke_configure(self, argin: str) -> Optional[Tuple[ResultCode, str]]:
        """This is a long running method for Configure command, it
        executes the do hook of the command class.

        :param argin: Input json string for the configure command
        :argin dtype: str
        """
        return self.do(argin)

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        try:
            result = kwargs.get("result")
            status = kwargs.get("status", TaskStatus.COMPLETED)
            message = kwargs.get(
                "exception", "Received exception in processing"
            )
            self.logger.info("Updating task status with kwargs: %s", kwargs)

            if status == TaskStatus.ABORTED:
                self.task_callback(
                    result=(ResultCode.ABORTED, "Command has been aborted"),
                    status=status,
                )
                return

            if result[0] == ResultCode.OK:
                self.component_manager.command_in_progress = None
                self.task_callback(result=result, status=status)

            else:
                self.task_callback(
                    result=result,
                    status=status,
                    exception=message,
                )
                self.component_manager.command_in_progress = None

            self.component_manager.command_id = ""
        except (KeyError, ValueError) as excepiton:
            self.logger.exception(
                "Exception occured while updating task status %s"
                "Received task_status",
                excepiton,
                kwargs,
            )
        self.logger.info("Updated task_status to: %s", kwargs)

    # pylint: disable=signature-differs
    def do(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Configure command on Mccs Subarray.

        :param argin: The string in JSON format.
        :argin dtype: str

        The JSON contains following values:

        Example:
        {"
        interface":
        "https://schema.skatelescope.org/ska-low-mccs-subarray-configure/3.0",
        "subarray_beams": [{"subarray_beam_id": 1,
        "update_rate": 0.0, "logical_bands": [{"start_channel": 80,
        "number_of_channels": 16}, {"start_channel": 384,
        "number_of_channels": 16}],
        "apertures": [{"aperture_id": "AP001.01",
        "weighting_key_ref": "aperture2"},
        {"aperture_id": "AP001.02", "weighting_key_ref": "aperture3"},
        {"aperture_id": "AP002.01", "weighting_key_ref": "aperture2"},
        {"aperture_id": "AP002.02", "weighting_key_ref": "aperture3"},
        {"aperture_id": "AP003.01", "weighting_key_ref": "aperture1"}],
        "sky_coordinates":{"timestamp": "2021-10-23T12:34:56.789Z",
        "reference_frame": "ICRS", "c1": 180.0, "c1_rate": 0.0, "c2": 45.0,
        "c2_rate": 0.0}}]}

        return:
            Tuple[ResultCode, str]:
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Subarray",
            self.mccs_subarray_adapter,
            "Configure",
            argin=argin,
        )
        self.logger.info(
            "%s Configure command -> ResultCode: %s and Message: %s",
            self.mccs_subarray_adapter.dev_name,
            result_code,
            unique_id_or_message,
        )
        return result_code[0], unique_id_or_message[0]
