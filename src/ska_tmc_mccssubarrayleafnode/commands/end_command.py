"""
End command class for LowTmcLeafNodeMccsSubarray.
"""
import time
from logging import Logger
from typing import Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class End(MccsSLNCommand):
    """
    This class implements the End command for MCCS Subarray Leaf Node device.
    """

    def __init__(self, component_manager, logger: Logger) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.task_callback: TaskCallbackType

    @timeout_tracker
    @error_propagation_tracker("get_obs_state", [ObsState.IDLE])
    def end(
        self,
    ):
        """This is a long running method for End command, it
        executes the do hook of the command class.
        """
        return self.do()

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        try:
            result = kwargs.get("result")
            status = kwargs.get("status", TaskStatus.COMPLETED)
            message = kwargs.get(
                "exception", "Received exception in processing"
            )
            self.logger.info("Updating task status with kwargs: %s", kwargs)

            if status == TaskStatus.ABORTED:
                self.task_callback(
                    result=(ResultCode.ABORTED, "Command has been aborted"),
                    status=status,
                )
                return
            if result[0] == ResultCode.OK:
                self.component_manager.command_in_progress = None
                self.task_callback(result=result, status=status)

            else:
                self.task_callback(
                    result=result,
                    status=status,
                    exception=message,
                )
                self.component_manager.command_in_progress = None

            self.component_manager.command_id = ""
        except (KeyError, ValueError) as exception:
            self.logger.exception(
                "Exception occured while updating task status %s"
                "Received task_status: %s",
                exception,
                kwargs,
            )
        self.logger.info("End command task status updated to: %s", kwargs)

    # pylint: disable=arguments-differ
    def do(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke End command on Mccs Subarray.

        :rtype: (ResultCode, message)
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Subarray",
            self.mccs_subarray_adapter,
            "End",
        )

        self.logger.info(
            "%s End command -> ResultCode: %s and Message: %s",
            self.mccs_subarray_adapter.dev_name,
            result_code,
            unique_id_or_message,
        )
        return result_code[0], unique_id_or_message[0]
