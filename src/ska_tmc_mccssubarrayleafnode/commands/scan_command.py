"""
Scan command class for LowTmcLeafNodeMccsSubarray.

"""
import time
from logging import Logger
from typing import Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class Scan(MccsSLNCommand):
    """
    This class implements the Scan command for MCCS Subarray Leaf Node device.

    It provides methods to invoke the scan command on the Mccs Subarray device
    and handle its execution.
    """

    def __init__(self, component_manager, logger: Logger) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger=self.logger
        )

    @timeout_tracker
    @error_propagation_tracker("is_scan_completed", [True])
    def scan(
        self,
        argin: str,
    ) -> None:
        """This method invoked the do hook of the command class.
            It also sets up the command tracking.

        :param argin: Input json string for the scan command
        :type argin: str
        """
        return self.do(argin)

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""

        try:
            result = kwargs.get("result")
            status = kwargs.get("status", TaskStatus.COMPLETED)
            message = kwargs.get(
                "exception", "Received exception in processing"
            )
            self.logger.info("Updating task status with kwargs: %s", kwargs)

            if status == TaskStatus.ABORTED:
                self.task_callback(
                    result=(ResultCode.ABORTED, "Command has been aborted"),
                    status=status,
                )
                return
            if result[0] == ResultCode.OK:
                self.component_manager.command_in_progress = None
                self.task_callback(result=result, status=status)

            else:
                self.task_callback(
                    result=result,
                    status=status,
                    exception=message,
                )
                self.component_manager.command_in_progress = None

            self.component_manager.command_id = ""
            self.component_manager.scan_result = None
        except (KeyError, ValueError) as exception:
            self.logger.exception(
                "Exception occured while updating task status %s",
                "Received task_status: %s",
                exception,
                kwargs,
            )
        self.logger.info("Scan command task status updated to: %s", kwargs)

    # pylint: disable=signature-differs
    def do(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on Mccs Subarray.

        :param argin: The string in JSON format.
        :argin dtype: str
            The JSON contains following values:

        Example:
        {
        "interface":
        "https://schema.skatelescope.org/ska-low-mccs-subarray-scan/3.0",
        "scan_id": 12345678,
        "start_time": "2023-12-31T12:34:28Z",
        "duration": 0.0
        }
        return:
            Tuple[ResultCode, str]:
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Subarray",
            self.mccs_subarray_adapter,
            "Scan",
            argin=argin,
        )
        self.logger.info(
            "%s Scan Command -> ResultCode: %s and Message: %s",
            self.mccs_subarray_adapter.dev_name,
            result_code,
            unique_id_or_message,
        )
        return result_code[0], unique_id_or_message[0]
