"""
Abort command class for LowTmcLeafNodeMccsSubarray.
"""
from typing import Tuple

from ska_tango_base.commands import ResultCode

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class Abort(MccsSLNCommand):
    """
    This class implements the Abort command for Mccs Subarray.
    It provides methods to Abort the Mccs Subarray device and
    handle the execution of the Abort command.
    """

    # pylint: disable=arguments-differ
    def do(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke Abort command on Mccs Subarray.

        :rtype: Tuple[ResultCode, str]
        """
        self.component_manager.command_in_progress = "Abort"
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, message = self.call_adapter_method(
            "MCCS Subarray",
            self.mccs_subarray_adapter,
            "Abort",
        )
        return result_code[0], message[0]
