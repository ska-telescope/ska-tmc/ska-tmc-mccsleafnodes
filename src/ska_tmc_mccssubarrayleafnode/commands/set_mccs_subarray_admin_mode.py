"""
SetAdminMode command class for MCCSSubarrayLeafNode.
"""

# pylint: disable=line-too-long
from typing import Tuple

from ska_control_model import AdminMode
from ska_tango_base.commands import ArgumentValidator, FastCommand, ResultCode

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (  # noqa: E501
    MccsSLNCommand,
)


# pylint: disable = abstract-method
class SetAdminMode(MccsSLNCommand, FastCommand):
    """
    A class for MCCSSubarrayLeafNode's SetAdminMode() command.

    SetAdminMode command on MCCSSubarrayLeafNode enables to set the adminMode
    of the MCCS subarray device
    """

    def __init__(self, component_manager, logger):
        """Initialization.

        Args:
            logger (logging.Logger): Used for logging.
            component_manager (MCCSSLNcomponentManager): Instance of
            MCCSSLNComponentManager.
        """
        super().__init__(component_manager, logger)
        self.component_manager = component_manager
        self._validator = ArgumentValidator()
        self._name = "SetAdminMode"

    # pylint: disable=signature-differs
    def do(self, argin: AdminMode) -> Tuple[ResultCode, str]:
        """
        A method to set the adminMode of the MCCS Subarray device
        :param argin: A adminMode enum value to be set for subarray device
        """
        if not self.component_manager.is_admin_mode_enabled:
            self.logger.info(
                "AdminMode functionality is disabled, "
                "Device will function normally."
            )
            return ResultCode.NOT_ALLOWED, (
                "AdminMode functionality is disabled, "
                "Device will function normally."
            )

        # Proceed with setting adminMode if enabled
        self.component_manager.command_in_progress = "SetAdminMode"
        return_code, message = self.init_adapter()
        if return_code == ResultCode.FAILED:
            return return_code, message

        try:
            self.mccs_subarray_adapter.adminMode = argin
            self.logger.info(
                "Invoking SetAdminMode command on %s",
                self.mccs_subarray_adapter.dev_name,
            )
        except Exception as e:
            self.logger.info(
                "Failed to set the adminMode of the MCCS Subarray. "
                "Error while setting the adminMode: %s",
                e,
            )
            return ResultCode.FAILED, "Command Failed"

        return ResultCode.OK, "Command Completed"
