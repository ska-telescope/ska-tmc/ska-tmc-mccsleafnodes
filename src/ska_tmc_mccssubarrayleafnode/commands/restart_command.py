"""
Restart command class for LowTmcLeafNodeMccsSubarray.

This module defines the Restart command class for the MCCS Subarray Leaf Node.
"""
import threading
from typing import Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus

from ska_tmc_mccssubarrayleafnode.commands.mccs_subarrayln_command import (
    MccsSLNCommand,
)


# pylint: disable=abstract-method
class Restart(MccsSLNCommand):
    """
    This class implements the Restart command for Mccs Subarray Leaf Node.
    It provides functionality to restart the MCCS Subarray device and handles
    the execution of the Restart command.
    """

    # pylint: disable=unused-argument
    def restart(
        self,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event],
    ) -> None:
        """
        Perform the Restart command for the MCCS Subarray leaf
        node device.

        :param task_callback: A callable function to update the command status.
        :param task_abort_event: A threading event to notify in case of an
            Abort command being called.
        """
        task_callback(status=TaskStatus.IN_PROGRESS)
        result_code, message = self.do()
        self.logger.info(message)
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=ResultCode.FAILED,
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=ResultCode.OK,
            )

    # The parent class implements the do method with an input argument, which
    # is not required by Restart command. Supressing the pylint warning here.
    # pylint: disable=arguments-differ
    def do(self) -> Tuple[ResultCode, str]:
        """
        Invoke the Restart command on the MCCS Controller device.

        :return: A tuple containing the result code and a message indicating
            the status of the Restart command execution.
        :rtype: Tuple[ResultCode, str]
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        argin: int = self.component_manager.subarray_id
        result_code, message = self.call_adapter_method(
            "MCCS Controller",
            self.mccs_master_adapter,
            "RestartSubarray",
            argin,
        )

        return result_code[0], message[0]
