"""

This module provides the implementation of the MccsSLNCommand class, which
is a common command class for MCCS Subarray Leaf Node. It contains common
functionality for the commands present on this device.

"""

import time
from logging import Logger
from typing import Any, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tmc_common import AdapterType, TmcLeafNodeCommand
from tango import ConnectionFailed, DevFailed


# pylint: disable=abstract-method
class MccsSLNCommand(TmcLeafNodeCommand):
    """
    Common Command class for MCCS Subarray Leaf Node. This class provides
    common functionality for the commands present on this device.
    """

    def __init__(
        self,
        component_manager,
        logger: Logger,
    ):
        """
        Initialize the MCCSSubarrayLeafNode Command instance.

        :param component_manager: The component manager for managing
        interactions with the MCCSSubarrayLeafNode
        :type component_manager: MccsSLNCComponentManager
        :param logger: An optional logger for logging messages
        :type logger: Optional
        """
        super().__init__(component_manager, logger)
        self.mccs_subarray_adapter = None
        self.mccs_master_adapter = None
        self.task_callback: TaskCallbackType
        self.lrcr_callback = (
            self.component_manager.long_running_result_callback
        )

    def set_command_id(self, command_name: str):
        """
        Sets the command id for error propagation.

        :param command_name: Name of the command
        :type command_name: str
        """
        command_id = f"{time.time()}-{command_name}"
        self.logger.info(
            "Setting command id as %s for command: %s",
            command_id,
            command_name,
        )
        self.component_manager.command_id = command_id
        self.component_manager.command_result[
            self.component_manager.command_id
        ] = None

    def init_adapter(self) -> Tuple[ResultCode, str]:
        """
        Initialize the adapters for communication with the MCCS Subarray device
        and the MCCS Master device.

        This method attempts to create adapters for both the MCCS Subarray
        device and the MCCS Master device using the specified device names
        obtained from the component manager. It continues trying until both
        adapters are created or the specified timeout is reached.

        :return: A tuple containing the result code and a message indicating
            the status of the adapter initialization.
        :rtype: Tuple[ResultCode, str]
        :raises ConnectionFailed: If there is an issue establishing a
            connection with the devices.
        :raises DevFailed: If there is a device-related error during adapter
            creation.
        :raises AttributeError: If there is an attribute error during adapter
            creation.
        :raises ValueError: If there is a value error during adapter creation.
        :raises TypeError: If there is a type error during adapter creation.
        """
        mccs_subarray_device_name = (
            self.component_manager.mccs_subarray_device_name
        )
        mccs_master_device_name = (
            self.component_manager.mccs_master_device_name
        )
        adapter_timeout = self.component_manager.adapter_timeout
        start_time = time.time()
        while (
            self.mccs_subarray_adapter is None
            or self.mccs_master_adapter is None
        ) and (time.time() - start_time) < adapter_timeout:
            try:
                self.mccs_subarray_adapter = (
                    self.adapter_factory.get_or_create_adapter(
                        mccs_subarray_device_name, AdapterType.SUBARRAY
                    )
                )
                self.mccs_master_adapter = (
                    self.adapter_factory.get_or_create_adapter(
                        mccs_master_device_name, AdapterType.MCCS_CONTROLLER
                    )
                )
            except (
                ConnectionFailed,
                DevFailed,
                AttributeError,
                ValueError,
                TypeError,
            ) as error:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    error_message = (
                        f"Error in creating adapters for"
                        f"{mccs_subarray_device_name} "
                        f"and {mccs_master_device_name}: {error}"
                    )
                    return ResultCode.FAILED, error_message

                message = (
                    f"Error in creating adapters for "
                    f"{mccs_subarray_device_name} "
                    f"and {mccs_master_device_name}: {error}"
                )
                return ResultCode.FAILED, message

        return ResultCode.OK, ""

    def do_low(self, argin: Optional[Any] = None):
        """Abstract Method from TmcLeafNodeCommand is
            defined here but not utilized by this Class.

        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """

    def update_task_status(self, **kwargs):
        """Abstract Method from TmcLeafNodeCommand is
        defined here but not utilized by this Class."""

    def do(self, argin: Optional[Any] = None):
        """Abstract Method from TmcLeafNodeCommand is
            defined here but not utilized by this Class.

        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None."""

    def init_adapter_low(self):
        """
        Initialize the adapter for the low-level device.
        """
        self.init_adapter()
