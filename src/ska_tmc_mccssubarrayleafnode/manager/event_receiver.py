"""
MCCS Subarray Leaf Node Event Receiver.

"""
from logging import Logger
from time import sleep

import tango
from ska_tango_base.control_model import ObsState
from ska_tmc_common import DeviceInfo
from ska_tmc_common.v1.event_receiver import EventReceiver
from tango import ConnectionFailed, DevFailed


class MccsSLNEventReceiver(EventReceiver):
    """
    The MccsSLNEventReceiver class is responsible for receiving events
    from the MCCS Subarray device.
    """

    def __init__(
        self,
        component_manager,
        logger: Logger,
        *,
        max_workers: int = 1,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
    ) -> None:
        """
        Initialize the MCCS Subarray Leaf Node Event Receiver.

        :param component_manager: The component manager associated with this
            event receiver.
        :param logger: Logger for logging events.
        :param max_workers: Maximum number of worker threads for
            concurrent tasks.
        :param proxy_timeout: Timeout in milliseconds for proxy
            operations.
        :param event_subscription_check_period: Time in seconds for sleep
            intervals in the event subsription thread.
        """
        super().__init__(
            component_manager,
            logger,
            max_workers,
            proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
        )
        self._subscribed = False

    def run(self) -> None:
        """
        The run method for the event receiver thread. Starts the operations of
        the event receiver.
        """
        with tango.EnsureOmniThread():
            while not self._subscribed:
                try:
                    device_info = self._component_manager.get_device()
                    self.subscribe_events(device_info)
                except (ConnectionFailed, DevFailed) as e:
                    self._logger.warning("Exception occurred: %s", e)
                sleep(self._event_subscription_check_period)

    # pylint: disable=arguments-differ

    def subscribe_events(self, dev_info: DeviceInfo) -> None:
        """Event subscription method. Subscribes to the events for the given
        device.

        :param dev_info: The device info object for the device on which
            events are to be subscribed.
        :dev_info dtype: DeviceInfo class object.
        """

        try:
            mccs_proxy = self._dev_factory.get_device(dev_info.dev_name)
        except DevFailed as exception:
            self._logger.exception(
                "Exception occured while creating the device proxy: %s",
                exception,
            )
        else:
            try:
                mccs_proxy.subscribe_event(
                    "obsState",
                    tango.EventType.CHANGE_EVENT,
                    self.handle_obs_state_event,
                    stateless=True,
                )
                mccs_proxy.subscribe_event(
                    "longRunningCommandResult",
                    tango.EventType.CHANGE_EVENT,
                    self.handle_command_result_event,
                    stateless=True,
                )
                mccs_proxy.subscribe_event(
                    "adminMode",
                    tango.EventType.CHANGE_EVENT,
                    self.handle_admin_mode_event,
                    stateless=True,
                )
                self._subscribed = True
                self._stop = True
            except (AttributeError, ValueError, TypeError, DevFailed) as e:
                self._logger.error(
                    "Exception occured while subscribing to event for %s: %s",
                    dev_info.dev_name,
                    e,
                )

    def handle_command_result_event(
        self, event: tango.EventType.CHANGE_EVENT
    ) -> None:
        """This method handles the longRunningCommandResult event."""
        if event.err:
            error = event.errors[0]
            self._logger.error(
                "Error occured while handling event for "
                + "longRunningCommandResult: %s - %s",
                error.reason,
                error.desc,
            )
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return

        new_value = event.attr_value.value
        device_name = event.device.dev_name()
        self._component_manager.update_long_running_command_result(
            device_name, new_value
        )

    def handle_obs_state_event(self, event: tango.EventData) -> None:
        """
        It handles the observation state events of different devices
        """
        if event.err:
            error = event.errors[0]
            error_msg = (
                f"Error handling LRCR event: {error.reason}, {error.desc}"
            )
            self._logger.error(error_msg)
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return

        new_value = event.attr_value.value
        self._component_manager.update_device_obs_state(
            event.device.dev_name(), new_value
        )
        self._logger.info(
            "Observation state value updated :%s", ObsState(new_value).name
        )
