"""
Component Manager class for MCCS Subarray Leaf Node
"""
import json
import threading
import time
from logging import Logger
from typing import Any, Callable, Optional, Tuple

import tango
from ska_control_model import AdminMode, HealthState
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    CommandNotAllowed,
    DeviceInfo,
    DeviceUnresponsive,
    LivelinessProbeType,
    LRCRCallback,
    SubArrayDeviceInfo,
)
from ska_tmc_common.v1.tmc_component_manager import TmcLeafNodeComponentManager

from ska_tmc_mccssubarrayleafnode.commands import (
    Abort,
    Configure,
    End,
    EndScan,
    Restart,
    Scan,
)
from ska_tmc_mccssubarrayleafnode.manager.event_receiver import (
    MccsSLNEventReceiver,
)


class MccsSLNComponentManager(TmcLeafNodeComponentManager):
    """
    A component manager for The LowTmcLeafNodeMccsSubarray device.

    It supports:

    * Monitoring its component, e.g. detect that it has been turned off
      or on
    """

    def __init__(
        self,
        mccs_subarray_device_name: str,
        mccs_master_device_name: str,
        logger: Logger,
        _liveliness_probe: LivelinessProbeType = (
            LivelinessProbeType.SINGLE_DEVICE
        ),
        *,  # Forces all arguments after this to be keyword-only
        _event_receiver: bool = True,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 2,
        command_timeout: int = 30,
        _update_availablity_callback: Optional[Callable] = None,
        _update_obs_state_callback: Optional[Callable] = None,
        _update_admin_mode_callback: Optional[Callable[[Any], None]] = None,
    ) -> None:
        """
        Initialize a new ComponentManager instance.

        :param mccs_subarray_device_name: (str) Name of the MCCS Subarray
        device.
        :param mccs_master_device_name: (str) Name of the MCCS Master
        device.
        :param logger: (Logger) A logger for this component manager.
        :param _liveliness_probe: (LivelinessProbeType) Type of liveliness
            probe.
        :param _event_receiver: (bool) Flag indicating whether event receiver
            should be started.
        :param proxy_timeout: (int) Timeout in milliseconds for proxy
            operations.
        :param event_subscription_check_period: (int) Time in seconds for sleep
            intervals in the event subsription thread.
        :param liveliness_check_period: (int) Period for the liveliness probe
            to monitor each device in a loop
        :param adapter_timeout: (int) Timeout for the adapter creation
        :param command_timeout: (int) Timeout for the command execution
        :param _update_availablity_callback: Callable
        :type update_availablity_callback: Callable function for updating
            device availability.
        """
        super().__init__(
            logger=logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=False,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
        )
        self._device = SubArrayDeviceInfo(mccs_subarray_device_name)
        self.mccs_controller_device = DeviceInfo(mccs_master_device_name)
        self.long_running_result_callback = LRCRCallback(self.logger)
        self.mccs_subarray_device_name: str = mccs_subarray_device_name
        self.mccs_master_device_name: str = mccs_master_device_name
        self.update_admin_mode_callback = _update_admin_mode_callback
        self.adapter_timeout: int = adapter_timeout
        self.rlock = threading.RLock()
        self.command_timeout: int = command_timeout
        self.command_mapping: dict = {}
        self.command_in_progress: str = ""
        self.rlock = threading._RLock()
        self.supported_commands: tuple = (
            "Configure",
            "End",
            "Scan",
            "EndScan",
        )
        self.update_availablity_callback = _update_availablity_callback
        if _event_receiver:
            evt_subscription_check_period = event_subscription_check_period
            self._event_receiver = MccsSLNEventReceiver(
                self,
                logger,
                proxy_timeout=proxy_timeout,
                event_subscription_check_period=evt_subscription_check_period,
            )
            self._event_receiver.start()
        if _liveliness_probe:
            self.start_liveliness_probe(_liveliness_probe)
        self.update_obs_state_callback = _update_obs_state_callback
        self._subarray_id: int = 0
        self.__set_subarray_id()
        self.command_result: dict = {}
        self.abort_event: threading.Event = threading.Event()
        self.scan_command_class: Scan = None
        self.scan_result: ResultCode = None

    def stop_scan_tracking(self):
        """Stop scan tracking and clean up resources."""
        if self.scan_command_class is not None:
            self.scan_command_class.timekeeper.stop_timer()
        else:
            self.logger.warning(
                "Scan command class not defined, cannot stop Timer"
            )
        self.scan_command_class = None

    @property
    def subarray_id(self) -> int:
        """Subarray ID for the subarray device associated with the Leaf Node"""
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self, id_to_set: int) -> None:
        """Setter method for setting the subarray ID

        :param id_to_set: Subarray ID of the subarray device
        :type id_to_set: int

        :rtype: None
        """
        self._subarray_id = id_to_set

    def __set_subarray_id(self) -> None:
        """A method to extract and set the Subarray ID from the device FQDN"""
        # Extract the subarray id from the device FQDN
        # Example: For a MCCS Subarray with FQDN - low-mccs/subarray/01 the
        # the subarray ID would be '01'
        try:
            subarray_id = self.mccs_subarray_device_name.split("/")[-1]
            self.subarray_id = int(subarray_id)
        except Exception as exception:
            # pylint: disable=fixme
            # TODO: Handle the exception in a better way to notify the user
            # that the FQDN might be incorrect.
            self.logger.exception(
                "Exception occurred while setting the Subarray ID: %s, "
                + "the FQDN might be incorrect: %s",
                exception,
                self.mccs_subarray_device_name,
            )

    def stop(self) -> None:
        """Stops the event receiver"""
        self._event_receiver.stop()

    def get_obs_state(self):
        """
        Get Current device obsState
        """
        return self.get_device().obs_state

    def get_device(self) -> SubArrayDeviceInfo:
        """
        Return the device info of the monitoring loop with name device_name

        :param None:
        :return: a device info
        :rtype: SubArrayDeviceInfo
        """
        return self._device

    def get_lrcr_result(self) -> ResultCode:
        """Returns long running command result for command
        with given command ID"""
        return self.command_result[self.command_id]

    # pylint: disable=arguments-differ

    def update_event_failure(self, device_name: str) -> None:
        """
        Update a monitored device failure status
        :param device_name: Name of the device
        :type device_name: str
        """
        self.logger.info("Event failure occured on %s", device_name)
        with self.rlock:
            self._device.last_event_arrived = time.time()
            self._device.update_unresponsive(False)

    # pylint: enable=arguments-differ

    def update_device_info(self, device_info: SubArrayDeviceInfo) -> None:
        """
        Update the device info used for monitoring.

        :param device_info: Information about the MCCS Subarray device.
        """
        self._device = device_info

    # pylint: disable=unused-argument
    def update_responsiveness_info(self, device_name: str = "") -> None:
        """
        Update a device with the correct availability information.

        :param dev_name: name of the device
        :type dev_name: str
        """
        with self.rlock:
            self._device.update_unresponsive(False, "")
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(True)

    def update_device_admin_mode(
        self, device_name: str, admin_mode: AdminMode
    ) -> None:
        """
        Update a monitored device admin mode,
        and call the relative callbacks if available
        :param device_name: Name of the device on which admin mode is updated
        :type device_name: str
        :param admin_state: admin mode of the device
        :type admin_mode: AdminMode
        """
        super().update_device_admin_mode(device_name, admin_mode)
        self.logger.info(
            "Admin Mode value updated to :%s", AdminMode(admin_mode).name
        )
        if self.update_admin_mode_callback:
            self.update_admin_mode_callback(admin_mode)

    def update_long_running_command_result(self, device_name: str, value: str):
        """Processes and updates the lrcr callback when a
        longRunningCommandResult event is received. The event data is processed
        and updated on the callback if the event is for an exception.

        :param device_name: Name of the device
        :device_name dtype: str
        :param value: The attribute value from the longRunningCommandResult
            event.
        :value dtype: Tuple of either (Unique ID, ResultCode) or (Unique ID,
            Exception message) type.
        """
        self.logger.info(
            "Received longRunningCommandResult event for device: %s, "
            + "with value: %s",
            device_name,
            value,
        )
        if value == ("", "") or not value:
            return
        try:
            unique_id, result_code_message = value
            result_code, message = json.loads(result_code_message)

            if result_code in [
                ResultCode.FAILED,
                ResultCode.NOT_ALLOWED,
                ResultCode.REJECTED,
            ]:
                if unique_id.endswith(self.supported_commands):
                    self.logger.debug(
                        "Updating LRCRCallback with value: %s for %s"
                        + "for device: %s",
                        value,
                        unique_id,
                        device_name,
                    )
                    self.long_running_result_callback(
                        self.command_id,
                        ResultCode.FAILED,
                        exception_msg=message,
                    )
                    self.observable.notify_observers(command_exception=True)
            if unique_id.endswith("Scan") and result_code == ResultCode.OK:
                self.scan_result = ResultCode.OK
                self.observable.notify_observers(attribute_value_change=True)
            elif result_code == ResultCode.FAILED:
                self.scan_result = ResultCode.FAILED
                self.logger.warning(
                    "Scan failed for device: %s with message: %s",
                    device_name,
                    message,
                )
        except Exception as exception:
            self.logger.error(
                "Exception has occurred while processing"
                "long running command result event: %s",
                exception,
            )

    # pylint: disable=arguments-differ
    def update_device_health_state(
        self, device_name: str, health_state: HealthState
    ) -> None:
        """
        Update a monitored device health state
        aggregate the health states available

        :param device_name: Name of the device
        :device_name dtype: str
        :param health_state: Health state of the device
        :health_state dtype: HealthState
        """
        self.logger.info("Updating device healthState on %s", device_name)
        with self.rlock:
            dev_info = self.get_device()
            dev_info.health_state = health_state
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)

    # pylint: disable=arguments-differ
    def update_device_state(
        self, device_name: str, state: tango.DevState
    ) -> None:
        """
        Update a monitored device state,
        aggregate the states available
        and call the relative callbacks if available

        :param state: state of the device
        :state dtype: DevState
        :param device_name: Name of the device
        :device_name dtype: str
        """
        self.logger.info("Updating device state on %s", device_name)
        with self.rlock:
            self._device.state = state
            self._device.last_event_arrived = time.time()
            self._device.update_unresponsive(False)

    # pylint: disable=arguments-differ
    def update_exception_for_unresponsiveness(
        self, device_info: DeviceInfo, exception: Exception
    ) -> None:
        """
        Mark a device as failed and invoke the corresponding callback
        if it exists

        :param device_info: a device info
        :device_info dtype: DeviceInfo
        :param exception: an exception
        :exception dtype: Exception
        """
        device_info.update_unresponsive(True, str(exception))
        with self.rlock:
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(False)

    def is_scan_completed(self):
        """
        A function to check whether Scan Command is completed succesfully."""
        if (
            self.get_device().obs_state == ObsState.SCANNING
            and self.scan_result == ResultCode.OK
        ):
            return True
        return False

    # pylint: disable=arguments-differ
    def update_device_obs_state(
        self, device_name: str, obs_state: ObsState
    ) -> None:
        """
        Update a monitored device obs state,
        and call the relative callbacks if available

        :param device_name: Name of the device
        :device_name dtype: str
        :param obs_state: Obs state of the device
        :obs_state dtype: ObsState
        """
        self.logger.info("Updating device obsState state on %s", device_name)
        with self.rlock:
            dev_info = self.get_device()
            dev_info.obs_state = obs_state
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.logger.info(
                "Obs State value updated to :%s", ObsState(obs_state).name
            )
            if self.update_obs_state_callback:
                self.update_obs_state_callback(obs_state)
            self.observable.notify_observers(attribute_value_change=True)

    def configure(
        self, argin: str, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the Configure command in the queue for execution.

        :param argin: input json string for configure command
        :argin dtype: str
        :param task_callable: A callable to update the command information.
        :task_callable dtype: TaskCallbackType
        :return: a TaskStatus and response
        """
        configure_command = Configure(self, logger=self.logger)
        task_status, response = self.submit_task(
            configure_command.invoke_configure,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("Configure"),
            task_callback=task_callback,
        )
        return task_status, response

    def end(self, task_callback: TaskCallbackType) -> Tuple[TaskStatus, str]:
        """
        Submits the End command in the queue for execution.

        :param task_callable: A callable to update the command information.
        :task_callable dtype: CalTaskCallbackTypelable
        :return: a TaskStatus and response
        """
        end_command = End(self, logger=self.logger)
        task_status, response = self.submit_task(
            end_command.end,
            is_cmd_allowed=self.is_command_allowed_callable("End"),
            task_callback=task_callback,
        )
        return task_status, response

    def scan(
        self, argin: str, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the Scan command in the queue for execution.

        :param argin: input json string for scan command
        :argin dtype: str
        :param task_callback: A callable to update the command information.
        :task_callback dtype: TaskCallbackType
        :return: a TaskStatus and response
        """

        # Create a Scan instance and submit the task
        scan_command = Scan(self, logger=self.logger)
        task_status, response = self.submit_task(
            scan_command.scan,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("Scan"),
            task_callback=task_callback,
        )
        return task_status, response

    def endscan(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the EndScan command in the queue for execution.

        :param task_callable: A callable to update the command information.
        :task_callable dtype: TaskCallbackType
        :return: a TaskStatus and response
        """
        endscan_command = EndScan(self, logger=self.logger)
        task_status, response = self.submit_task(
            endscan_command.endscan,
            is_cmd_allowed=self.is_command_allowed_callable("EndScan"),
            task_callback=task_callback,
        )
        return task_status, response

    def abort_commands(self) -> Tuple[ResultCode, str]:
        """
        Invokes Abort command on Mccs Subarray
        and changes the obsState

        :param task_callback: callback to be called whenever the status
            of the task changes.
        """
        abort_command = Abort(
            self,
            logger=self.logger,
        )
        self.abort_event.set()
        self.observable.notify_observers(attribute_value_change=True)
        result_code, message = abort_command.do()
        self.abort_event.clear()
        return result_code, message

    def restart(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the Restart command in the queue for execution.

        :param task_callable: A callable to update the command information.
        :task_callable dtype: TaskCallbackType
        :return: a TaskStatus and response
        """
        restart_command = Restart(self, logger=self.logger)
        task_status, response = self.submit_task(
            restart_command.restart,
            is_cmd_allowed=self.is_command_allowed_callable("Restart"),
            task_callback=task_callback,
        )
        return task_status, response

    def _check_if_mccs_subarray_is_responsive(self) -> None:
        """Checks if MCCS Subarray device is responsive."""

        if self._device is None or self._device.unresponsive:
            raise DeviceUnresponsive

    def is_command_allowed(self, command_name: str) -> bool:
        """
        Checks whether the specified command is allowed to be executed on this
        device.

        This method ensures that the device is not in the FAULT or UNKNOWN
        state before executing the command. It also verifies that all the
        components needed for the operation are responsive.

        :param command_name: The name of the command to be checked.
        :type command_name: str
        :return: True if the command is allowed, False otherwise.
        :rtype: bool
        :raises CommandNotAllowed: If the specified command is not allowed.
        """
        ALLOWED_OBS_STATES = {
            "Configure": [ObsState.IDLE, ObsState.READY],
            "End": [ObsState.IDLE, ObsState.READY],
            "Scan": [ObsState.READY],
            "EndScan": [ObsState.SCANNING],
            "Abort": [
                ObsState.SCANNING,
                ObsState.CONFIGURING,
                ObsState.RESOURCING,
                ObsState.IDLE,
                ObsState.READY,
            ],
            "Restart": [
                ObsState.FAULT,
                ObsState.ABORTED,
            ],
        }

        self._check_if_mccs_subarray_is_responsive()

        allowed_obs_states = ALLOWED_OBS_STATES.get(command_name)
        if allowed_obs_states and self.get_obs_state() in allowed_obs_states:
            return True

        raise CommandNotAllowed(
            f"{command_name} command on this "
            + "device is not allowed.\n"
            + "Reason: The current Observation State "
            + f"is {self.get_obs_state()}."
        )

    def on(self):
        """Method 'on' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def off(self):
        """Method 'off' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def standby(self):
        """Method 'standby' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def start_communicating(self):
        """Method 'start_communicating' is abstract in class
        'BaseComponentManager' but is not overridden in child class '
        MccsSLNComponentManager'"""

    def stop_communicating(self):
        """Method 'stop_communicating' is abstract in class
        'BaseComponentManager' but is not overridden in child class '
        MccsSLNComponentManager'"""

    def is_command_allowed_callable(self, command_name: str) -> None:
        """
        Args:
            command_name (str): Name for the command for which the is_allowed
                check need to be applied.
        """
        self._check_if_mccs_subarray_is_responsive()

        def check_obs_state():
            """Return whether the command may be called in the current state.

            Returns:
                bool: whether the command may be called in the current device
                state
            """
            command_allowed_obsstates = {
                "Configure": [ObsState.IDLE, ObsState.READY],
                "End": [ObsState.READY],
                "Scan": [ObsState.READY],
                "EndScan": [ObsState.SCANNING],
                "Abort": [
                    ObsState.SCANNING,
                    ObsState.CONFIGURING,
                    ObsState.RESOURCING,
                    ObsState.IDLE,
                    ObsState.READY,
                ],
                "Restart": [ObsState.FAULT, ObsState.ABORTED],
            }
            if command_name in command_allowed_obsstates:
                if (
                    self.get_obs_state()
                    not in command_allowed_obsstates[command_name]
                ):
                    return False
            return True

        return check_obs_state
