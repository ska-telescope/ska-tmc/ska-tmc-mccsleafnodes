"""
MCCSSubarrayLeafnode acts as a contact point for the MCCS Subarray Node
and also monitors and issues commands to the MCCS Subarray.
"""
# pylint: disable = line-too-long
from typing import List, Tuple

from ska_control_model import AdminMode, HealthState
from ska_tango_base.base.base_device import SKABaseDevice
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tango_base.control_model import ObsState
from ska_tmc_common import LivelinessProbeType
from ska_tmc_common.v1.tmc_base_leaf_device import TMCBaseLeafDevice
from tango import AttrWriteType, DebugIt
from tango.server import attribute, command, device_property, run

from ska_tmc_mccssubarrayleafnode import release
from ska_tmc_mccssubarrayleafnode.commands.set_mccs_subarray_admin_mode import (  # noqa: E501
    SetAdminMode,
)

# pylint: enable = line-too-long
from ska_tmc_mccssubarrayleafnode.manager.component_manager import (
    MccsSLNComponentManager,
)


class LowTmcLeafNodeMccsSubarray(TMCBaseLeafDevice):
    """
    MCCS Subarray Leaf node acts as a contact point for MCCS Subarray Node
    and also monitors and issues commands to the MCCS Subarray.
    """

    # pylint: disable= protected-access
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_subsystem_available = False
        self._mccs_subarray_obs_state = ObsState.EMPTY

    # -----------------
    # Device Properties
    # -----------------

    MccsMasterFQDN = device_property(
        dtype="str",
        doc="FQDN of the MCCS Master Tango Device Server.",
    )

    MccsSubarrayFQDN = device_property(
        dtype="str",
        doc="FQDN of the MCCS Subarray Tango Device Server.",
    )
    CommandTimeOut = device_property(dtype="DevFloat", default_value=30)

    # -----------------
    # Attributes
    # -----------------

    isSubsystemAvailable = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
    )

    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)

    mccsSubarrayAdminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ,
    )

    mccsSubarrayDevName = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
    )
    obsState = attribute(dtype=ObsState, access=AttrWriteType.READ)

    class InitCommand(SKABaseDevice.InitCommand):
        """
        A class for the TMC LowTmcLeafNodeMccsSubarray's init_device() method.
        """

        def do(self, *args, **kwargs) -> Tuple[ResultCode, str]:
            """
            Initializes the attributes and properties of the
            LowTmcLeafNodeMccsSubarray.

            :return: A tuple containing a return code and a string message
                    indicating status. The message is for information purposes
                    only.
            :rtype: (ResultCode, str)
            """
            super().do(*args, **kwargs)
            device = self._device

            device._build_state = (
                f"{release.name},{release.version},{release.description}"
            )
            device._health_state = HealthState.OK
            device._version_id = release.version
            device._is_subsystem_available = False
            device.op_state_model.perform_action("component_on")
            device._admin_mode = AdminMode.ONLINE
            device._mccs_subarray_leaf_node_admin_mode = AdminMode.ONLINE
            for attribute_name in [
                "healthState",
                "isSubsystemAvailable",
                "obsState",
                "mccsSubarrayAdminMode",
                "isAdminModeEnabled",
            ]:
                device.set_change_event(attribute_name, True, False)
                device.set_archive_event(attribute_name, True)
            return (
                ResultCode.OK,
                "LowTmcLeafNodeMccsSubarray initialized successfully.",
            )

    def always_executed_hook(self) -> None:
        pass

    def delete_device(self) -> None:
        """
        Clean up resources and stop any running components when the device is
          deleted.
        """
        if hasattr(self, "component_manager"):
            self.component_manager.stop_event_receiver()
            self.component_manager.stop_liveliness_probe()
        self.logger.info("Resources cleaned up and component manager stopped.")

    # ------------------
    # Attributes methods
    # ------------------
    def update_availablity_callback(self, availablity: bool) -> None:
        """Change event callback for isSubsystemAvailable."""
        if availablity != self._is_subsystem_available:
            self._is_subsystem_available = availablity
            self.push_change_archive_events(
                "isSubsystemAvailable", self._is_subsystem_available
            )

    def update_mccs_subarray_obs_state_callback(
        self, obs_state: ObsState
    ) -> None:
        """Change event callback for obsState."""
        self._mccs_subarray_obs_state = obs_state
        self.push_change_archive_events(
            "obsState", self._mccs_subarray_obs_state
        )

    # pylint: disable= attribute-defined-outside-init
    def update_admin_mode_callback(self, admin_mode: int):
        """Update mccsSubarrayAdminMode attribute callback"""
        self._mccs_subarray_leaf_node_admin_mode = admin_mode

        try:
            self.push_change_archive_events(
                "mccsSubarrayAdminMode",
                self._mccs_subarray_leaf_node_admin_mode,
            )
            self.logger.info(
                "Successfully updated and pushed mccsSubarrayAdminMode "
                "attribute value to: %s",
                admin_mode,
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for "
                + "mccsSubarrayAdminMode: %s",
                exception,
            )

    def read_isSubsystemAvailable(self) -> bool:
        """Returns the isSubsystemAvailable attribute.

        :return: The availability status of the subsystem.
        :rtype: bool
        """
        return self._is_subsystem_available

    def read_mccsSubarrayDevName(self) -> str:
        """Returns the mccssubarraydevname attribute.

        :return: The MCCS Subarray device name.
        :rtype: str
        """
        return self.component_manager.mccs_subarray_device_name

    def write_mccsSubarrayDevName(self, value: str) -> None:
        """Sets the mccssubarraydevname attribute.

        :param value: The new device name for the MCCS Subarray.
        :type value: str
        """
        self.component_manager.mccs_subarray_device_name = value

    def read_mccsSubarrayAdminMode(self) -> int:
        """Read method for mccsControllerAdminMode Attribute"""
        return self._mccs_subarray_leaf_node_admin_mode

    def read_isAdminModeEnabled(self) -> bool:
        """Return the isAdminModeEnabled attribute value"""
        return self.component_manager.is_admin_mode_enabled

    def write_isAdminModeEnabled(self, value: bool):
        """Set the value of isAdminModeEnabled attribute"""
        self.component_manager.is_admin_mode_enabled = value

    def read_obsState(self) -> ObsState:
        """Read method for obsState.

        :return: The observation state.
        :rtype: ObsState
        """
        return self._mccs_subarray_obs_state

    def is_Configure_allowed(self) -> bool:
        """
        Checks whether Configure command is allowed to be run in
        the current device state.

        :return: True if Configure command is allowed.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("Configure")

    @command(
        dtype_in="str",
        doc_in="JSON-formatted string containing the subarray ID and "
        "resources to be allocated.",
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a list of result codes and a list of "
        "unique IDs.",
    )
    @DebugIt()
    def Configure(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Configure command on the MCCS Subarray.

        :param argin: JSON-formatted string containing the subarray ID and
         resources to be allocated.
        :type argin: str
        :return: A tuple containing a list of result codes and a list of
         unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Received Configure command with input :%s", argin)
        handler = self.get_command_object("Configure")
        result_code, unique_id = handler(argin)
        self.logger.debug(
            "Configure command result: %s, ID: %s.", result_code, unique_id
        )
        return ([result_code], [unique_id])

    def is_End_allowed(self) -> bool:
        """
        Checks whether End command is allowed to be run in the current device
          state.

        :return: True if End command is allowed.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("End")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a list of result codes and a list of "
        "unique IDs.",
    )
    @DebugIt()
    def End(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the End command on the MCCS Subarray to end the current
         scheduling block.

        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Received End command")
        handler = self.get_command_object("End")
        result_code, unique_id = handler()
        self.logger.debug(
            "End command result: %s, ID: %s.", result_code, unique_id
        )
        return ([result_code], [unique_id])

    def is_Scan_allowed(self) -> bool:
        """
        Checks whether Scan command is allowed to be run in the current device
          state.

        :return: True if Scan command is allowed.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("Scan")

    @command(
        dtype_in="str",
        doc_in="JSON-formatted string containing scan configuration.",
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a list of result codes and a list of "
        "unique IDs.",
    )
    @DebugIt()
    def Scan(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Scan command on the MCCS Subarray.

        :param argin: JSON-formatted string containing scan configuration.
        :type argin: str
        :return: A tuple containing a list of result codes and a list of "
        "unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Received Scan command with input: %s", argin)
        handler = self.get_command_object("Scan")
        result_code, unique_id = handler(argin)
        self.logger.debug(
            "Scan command result: %s, ID: %s.", result_code, unique_id
        )
        return ([result_code], [unique_id])

    def is_EndScan_allowed(self) -> bool:
        """
        Checks whether EndScan command is allowed to be run in current device
        state.

        return:
            True if EndScan command is allowed to be run in current device
            state.

        rtype:
            boolean

        """
        return self.component_manager.is_command_allowed("EndScan")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def EndScan(self) -> Tuple[List[ResultCode], List[str]]:
        """This command invokes EndScan command on Mccs Subarray."""
        handler = self.get_command_object("EndScan")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    def is_Abort_allowed(self) -> bool:
        """
        Checks whether Abort command is allowed to be run in the current device
          state.

        :return: True if Abort command is allowed.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("Abort")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a list of result codes and a list of "
        "unique IDs.",
    )
    @DebugIt()
    def Abort(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Abort command on the MCCS Subarray.

        :return: A tuple containing a list of result codes and a list of
         unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Received Abort command")
        handler = self.get_command_object("Abort")
        result_code, unique_id = handler()
        self.logger.debug(
            "Abort command result: %s, ID: %s.", result_code, unique_id
        )
        return ([result_code], [unique_id])

    def is_Restart_allowed(self) -> bool:
        """
        Checks whether Restart command is allowed to be run in the current
        device state.

        :return: True if Restart command is allowed.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("Restart")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="A tuple containing a list of result codes and a list of "
        "unique IDs.",
    )
    @DebugIt()
    def Restart(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Restart command on the MCCS Subarray.

        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Received Restart command")
        handler = self.get_command_object("Restart")
        result_code, unique_id = handler()
        self.logger.debug(
            "Restart command result: %s, ID: %s.", result_code, unique_id
        )
        return ([result_code], [unique_id])

    @command(
        dtype_in=AdminMode,
        doc_in="The adminMode in enum format",
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def SetAdminMode(self, argin: AdminMode):
        """
        This command sets the adminMode command on MCCS Subarray.
        """
        handler = self.get_command_object("SetAdminMode")
        result_code, message = handler(argin)
        self.logger.debug(
            "SetAdminMode command result: %s, Message: %s.",
            result_code,
            message,
        )
        return [result_code], [message]

    def init_command_objects(self) -> None:
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()

        for command_name, method_name in [
            ("Configure", "configure"),
            ("End", "end"),
            ("Scan", "scan"),
            ("EndScan", "endscan"),
            ("Restart", "restart"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=None,
                ),
            )
        self.register_command_object(
            "Abort",
            self.AbortCommandsCommand(self.component_manager, self.logger),
        )
        self.register_command_object(
            "SetAdminMode",
            SetAdminMode(self.component_manager, self.logger),
        )

    def create_component_manager(self):
        """Returns Mccs Subarray Leaf Node component manager object"""
        cm = MccsSLNComponentManager(
            self.MccsSubarrayFQDN,
            self.MccsMasterFQDN,
            logger=self.logger,
            _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
            _event_receiver=True,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            command_timeout=self.CommandTimeOut,
            _update_availablity_callback=self.update_availablity_callback,
            _update_obs_state_callback=(
                self.update_mccs_subarray_obs_state_callback
            ),
            _update_admin_mode_callback=self.update_admin_mode_callback,
        )
        return cm


# Entry point for the Tango device server
def main(args=None, **kwargs):
    """
    Entry point for running the Tango device server.

    :param args: Additional arguments for the device server.
    :type args: list
    :param kwargs: Additional keyword arguments for the device server.
    :type kwargs: dict
    """
    return run((LowTmcLeafNodeMccsSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
