# flake8: noqa
"""
This file is part of the MCCS Master Leaf Node project
and defines constant used
"""

from ska_tango_base.commands import ResultCode

MCCS_RELEASE_VERSION = (
    "https://schema.skatelescope.org/ska-low-mccs-controller-release/2.0"
)
MCCS_ASSIGN_VERSION = (
    "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0"
)


REQUIRED_LOW_ASSIGN_RESOURCE_KEYS = ["subarray_id"]

RESULTCODE_OK = (ResultCode.OK, "Command Completed")
MCCSMLN_RESULTCODE_FAILED = [ResultCode.FAILED, ResultCode.REJECTED]
OBS_RESULTCODE_FAILED = [
    ResultCode.FAILED,
    ResultCode.REJECTED,
    ResultCode.NOT_ALLOWED,
]
