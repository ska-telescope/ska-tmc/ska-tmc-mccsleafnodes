"""Event Receiver for Mccs Master Leaf Node"""
from logging import Logger
from time import sleep

import tango
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.v1.event_receiver import EventReceiver
from tango import ConnectionFailed, DevFailed


class MccsMLNEventReceiver(EventReceiver):
    """
    The MccsMLNEventReceiver class has the responsibility to receive events
    from the MCCS controller .

    The ComponentManager uses the handle events methods
    for the attribute of interest.
    For each of them a callback is defined.

    """

    def __init__(
        self,
        component_manager,
        logger=Logger,
        *,
        max_workers: int = 1,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
    ) -> None:
        super().__init__(
            component_manager,
            logger,
            max_workers,
            proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
        )
        self._max_workers = max_workers
        self._event_subscription_check_period = event_subscription_check_period
        self._component_manager = component_manager
        self._subscribed = False

    def run(self) -> None:
        with tango.EnsureOmniThread():
            while not self._subscribed:
                try:
                    devInfo = self._component_manager.get_device()
                    self.subscribe_events(devInfo)
                    sleep(self._event_subscription_check_period)
                except (ConnectionFailed, DevFailed) as exception:
                    self._logger.exception("Exception occurred: %s", exception)

    # pylint: disable=arguments-differ
    def subscribe_events(self, dev_info: DeviceInfo) -> None:
        try:
            mccs_proxy = self._dev_factory.get_device(dev_info.dev_name)
        except DevFailed as exception:
            self._logger.exception(
                "Exception occurred while getting proxy: %s", exception
            )
        else:
            try:
                mccs_proxy.subscribe_event(
                    "longRunningCommandResult",
                    tango.EventType.CHANGE_EVENT,
                    self.handle_command_result_event,
                    stateless=True,
                )
                mccs_proxy.subscribe_event(
                    "adminMode",
                    tango.EventType.CHANGE_EVENT,
                    self.handle_admin_mode_event,
                    stateless=True,
                )
                self._subscribed = True
            except (
                AttributeError,
                ValueError,
                TypeError,
                DevFailed,
            ) as exception:
                self._logger.exception(
                    "Exception occurred while subscribing to event: %s",
                    exception,
                )

    def handle_command_result_event(
        self, event: tango.EventType.CHANGE_EVENT
    ) -> None:
        """Handle the LRCR event."""

        if event.err:
            error = event.errors[0]
            error_msg = f"{error.reason},{error.desc}"
            self._logger.error(error_msg)
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return

        new_value = event.attr_value.value
        self._component_manager.update_long_running_command_result(
            event.device.dev_name(), new_value
        )
