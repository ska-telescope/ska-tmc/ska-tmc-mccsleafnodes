# pylint: disable= arguments-differ
"""
This module implements ComponentManager class for the Mccs Master Leaf Node.
"""
import json
import threading
from logging import Logger
from typing import Any, Callable, Optional, Tuple

from ska_control_model import AdminMode, TaskStatus
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceInfo,
    DeviceUnresponsive,
    LivelinessProbeType,
    LRCRCallback,
)
from ska_tmc_common.adapters import AdapterFactory
from ska_tmc_common.v1.tmc_component_manager import TmcLeafNodeComponentManager
from tango import DevState

from ska_tmc_mccsmasterleafnode.commands import (
    AssignResources,
    Off,
    On,
    ReleaseAllResources,
    Standby,
)
from ska_tmc_mccsmasterleafnode.manager.event_receiver import (
    MccsMLNEventReceiver,
)
from ska_tmc_mccsmasterleafnode.utils.constants import (
    OBS_RESULTCODE_FAILED,
    REQUIRED_LOW_ASSIGN_RESOURCE_KEYS,
)


class MccsMLNComponentManager(TmcLeafNodeComponentManager):
    """
     A component manager for MCCSMasterLeafNode component.

    It supports:

    * Monitoring its component, e.g. detect that it has been turned off
      or on
    * Controlling the behaviour of MCCS Master.
    """

    def __init__(
        self,
        mccs_master_device_name: str,
        logger: Logger,
        _liveliness_probe: LivelinessProbeType = (
            LivelinessProbeType.SINGLE_DEVICE
        ),
        *,  # Forces all arguments after this to be keyword-only
        _event_receiver: bool = False,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 2,
        _update_availablity_callback: Optional[Callable[[bool], None]] = None,
        _update_admin_mode_callback: Optional[Callable[[Any], None]] = None,
        command_timeout: int = 15,
    ):
        """
        Initialise a new ComponentManager instance.
        :param mccs_master_dev_name: Name of the MCCS Master device.
        :type mccs_master_dev_name:str
        :param logger: Optional. A logger for this component manager
        :type logger: logging.Logger
        :param _liveliness_probe: Optional. LivelinessProbe for the
          component manager
        :type _liveliness_probe: LivelinessProbeType
        :param _event_receiver: Optional. Object of EventReceiver class
        :type _event_receiver:bool
        :param proxy_timeout: Optional. Time period to wait for event and
          responses. Default 500 milliseconds
        :type proxy_timeout: int
        :param event_subscription_check_period: Time in seconds for sleep
            intervals in the event subsription thread.
        :type event_subscription_check_period: int
        :param liveliness_check_period: Period for the liveliness probe to
            monitor each device in a loop
        :type liveliness_check_period: int
        :param timeout: Maximum time in seconds for device responsiveness
          checks.
        :type timeout: int
        :param _update_availablity_callback: Callback function for updating
          device availability.
        :type update_availablity_callback: Callable

        """
        super().__init__(
            logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=False,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
        )
        self._mccs_master_device_name = mccs_master_device_name
        self._device = DeviceInfo(mccs_master_device_name)
        self.adapter_timeout: int = adapter_timeout
        self.rlock = threading.RLock()
        self.update_availablity_callback = _update_availablity_callback
        self.update_admin_mode_callback = _update_admin_mode_callback
        self.command_mapping: dict = {}
        self.on_command_object = On(self, logger)
        self.off_command_object = Off(self, logger)
        self.long_running_result_callback = LRCRCallback(self.logger)
        self.command_timeout: int = command_timeout
        self.command_in_progress: str = ""
        self.command_id: str = ""
        if _liveliness_probe:
            self.start_liveliness_probe(_liveliness_probe)
        if _event_receiver:
            evt_subscription_check_period = event_subscription_check_period
            self._event_receiver = MccsMLNEventReceiver(
                self,
                logger,
                proxy_timeout=proxy_timeout,
                event_subscription_check_period=evt_subscription_check_period,
            )
            self._event_receiver.start()
        self.command_result: dict = {}
        self.adapter_factory = AdapterFactory()

    def get_device(self) -> DeviceInfo:
        return self._device

    @property
    def mccs_master_device_name(self) -> str:
        """Returns device name for the MCCS Master Device."""
        return self._mccs_master_device_name

    @mccs_master_device_name.setter
    def mccs_master_device_name(self, device_name: str) -> None:
        """Sets the device name for MCCS Master Device."""
        self._mccs_master_device_name = device_name

    def clear_command_mapping(self, unique_id: str) -> None:
        """Clear command mapping for given unique id."""
        if self.command_mapping.get(unique_id):
            del self.command_mapping[unique_id]

    def update_device_admin_mode(
        self, device_name: str, admin_mode: AdminMode
    ) -> None:
        """
        Update a monitored device admin mode,
        and call the relative callbacks if available
        :param device_name: Name of the device on which admin mode is updated
        :type device_name: str
        :param admin_state: admin mode of the device
        :type admin_mode: AdminMode
        """
        super().update_device_admin_mode(device_name, admin_mode)
        self.logger.info(
            "Admin Mode value updated to :%s", AdminMode(admin_mode).name
        )
        if self.update_admin_mode_callback:
            self.update_admin_mode_callback(admin_mode)

    # pylint: disable= unused-argument
    def update_responsiveness_info(self, device_name: str = "") -> None:
        """
        Update a device with the correct availability information.

        :param dev_name: name of the device
        :type dev_name: str
        """
        with self.rlock:
            self.get_device().update_unresponsive(False, "")
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(True)

    def update_exception_for_unresponsiveness(
        self, device_info: DeviceInfo, exception: Exception
    ) -> None:
        """
        Mark a device as failed and invoke the corresponding callback
        if it exists.

         :param device_info: a device info
         :type device_info: DeviceInfo
         :param exception: an exception
         :type: Exception
        """
        device_info.update_unresponsive(True, str(exception))
        with self.rlock:
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(False)

    def is_mccs_master_responsive(self) -> None:
        """Checks if MCCS Master device is responsive."""

        if self._device is None or self._device.unresponsive:
            raise DeviceUnresponsive(
                f"{self.mccs_master_device_name} not available"
            )

    def validate_json_argument_low(self, json_argument, req_keys) -> tuple:
        """To validate the low json for assign resources command before
        entering the queue
        Args:
            json_argument (dict): Json Argument
            req_keys (list): Required key list to check in json argument
        """
        json_keys = json_argument.keys()
        for key in req_keys:
            if key not in json_keys:
                return (
                    False,
                    f"{key} key is not present in the input json argument.",
                )

        return True, "Received json has required fields"

    def get_lrcr_result(self) -> ResultCode:
        """Returns long running command result for command
        with given command ID"""
        return self.command_result[self.command_id]

    def update_long_running_command_result(self, device_name: str, value: str):
        """Processes and updates the lrcr callback when a
        longRunningCommandResult event is received. The event data is processed
        and updated on the callback if the event is for an exception.

        :param device_name: Name of the device
        :device_name dtype: str
        :param value: The attribute value from the longRunningCommandResult
            event.
        :value dtype: Tuple of either (Unique ID, ResultCode) or (Unique ID,
            Exception message) type.
        """
        self.logger.info(
            "Received longRunningCommandResult event for device: %s, "
            "with value: %s",
            device_name,
            value,
        )
        self.logger.info(
            "Updating long running command result -> "
            "Unique ID and result_code_or_exception: %s"
            " Command Mapping: %s",
            value,
            self.command_mapping,
        )
        try:
            unique_id, result_code_or_exception = value
        except (ValueError, TypeError) as exception:
            self.logger.exception(
                "Exception occured while evaluating value: %s", exception
            )
        else:
            if unique_id in self.command_mapping:
                if not result_code_or_exception:
                    # Ignoring an empty event, or an event with ResultCode
                    pass
                else:
                    result_code, message = json.loads(result_code_or_exception)

                    if result_code == ResultCode.OK:
                        command_id = self.command_mapping[unique_id]
                        self.command_result[command_id] = ResultCode.OK

                        self.observable.notify_observers(
                            attribute_value_change=True
                        )
                    elif result_code in OBS_RESULTCODE_FAILED:
                        exception_message = (
                            "Exception occurred on device:"
                            f" {device_name}: {message}"
                        )
                        self.long_running_result_callback(
                            self.command_mapping[unique_id],
                            ResultCode.FAILED,
                            exception_msg=exception_message,
                        )
                        self.observable.notify_observers(
                            command_exception=True
                        )

    def is_command_allowed(self, command_name: str) -> bool:
        """
        Checks whether this command is allowed.
        It checks that the device is not in the FAULT and UNKNOWN state
        before executing the command and that all the
        components needed for the operation are not unresponsive.

        :return: True if this command is allowed

        :rtype: boolean
        """
        if command_name in [
            "On",
            "Off",
            "Standby",
            "AssignResources",
            "ReleaseAllResources",
        ]:
            if self.op_state_model.op_state in [
                DevState.FAULT,
                DevState.UNKNOWN,
            ]:
                raise CommandNotAllowed(
                    f"The invocation of the {__class__} command on this "
                    + "device is not allowed.\n"
                    + "Reason: The current operational state "
                    + f"is {self.op_state_model.op_state}."
                    + "The command has NOT been executed."
                    + "This device will continue with normal operation.",
                    self.op_state_model.op_state,
                )
            self.is_mccs_master_responsive()
            return True
        return False

    def on_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """Submits the On command for execution.

        :rtype: tuple
        """
        on_command = On(self, self.logger)
        task_status, response = self.submit_task(
            on_command.on,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("On"),
            task_callback=task_callback,
        )
        self.logger.debug(
            "Taskstatus: %s, Response: %s of On command:",
            task_status,
            response,
        )
        return task_status, response

    def off_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """Submits the Off command for execution.

        :rtype: tuple
        """
        off_command = Off(self, self.logger)
        task_status, response = self.submit_task(
            off_command.off,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("Off"),
            task_callback=task_callback,
        )
        self.logger.debug(
            "Taskstatus: %s, Response: %s of Off command:",
            task_status,
            response,
        )
        return task_status, response

    def assign_resources(
        self, argin, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits AssignResources command as a separate task.

        :return: a result code and message
        """

        json_argument = json.loads(argin)

        assign_resources_command = AssignResources(self, logger=self.logger)

        (
            is_valid,
            invalid_json_error_msg,
        ) = self.validate_json_argument_low(
            json_argument, REQUIRED_LOW_ASSIGN_RESOURCE_KEYS
        )
        if not is_valid:
            return assign_resources_command.reject_command(
                invalid_json_error_msg
            )

        task_status, response = self.submit_task(
            assign_resources_command.assign_resources,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("AssignResources"),
            task_callback=task_callback,
        )
        return task_status, response

    def release_resources(
        self, argin, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits ReleaseAllResources command as a separate task.

        :return: a result code and message
        """
        release_all_resources_command = ReleaseAllResources(
            self, logger=self.logger
        )

        task_status, response = self.submit_task(
            release_all_resources_command.release_all_resources,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable(
                "ReleaseAllResources"
            ),
            task_callback=task_callback,
        )
        return task_status, response

    def standby_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the Standby command for execution.
        :rtype: tuple
        """
        standby_command = Standby(self, self.logger)
        task_status, response = self.submit_task(
            standby_command.standby,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("Standby"),
            task_callback=task_callback,
        )
        self.logger.debug(
            "Taskstatus: %s, Response: %s of Standby command:",
            task_status,
            response,
        )
        return task_status, response

    def on(self):
        """Method 'on' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def off(self):
        """Method 'off' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def standby(self):
        """Method 'standby' is abstract in class 'BaseComponentManager'
        but is not overridden in child class 'MccsSLNComponentManager'"""

    def start_communicating(self):
        """Method 'start_communicating' is abstract in class
        'BaseComponentManager' but is not overridden in child class '
        MccsSLNComponentManager'"""

    def stop_communicating(self):
        """Method 'stop_communicating' is abstract in class
        'BaseComponentManager' but is not overridden in child class '
        MccsSLNComponentManager'"""

    def is_command_allowed_callable(self, command_name: str):
        """
        Args:
            command_name (str): Name for the command for which the is_allowed
                check need to be applied.
        """

        self.get_device().state = self.adapter_factory.get_or_create_adapter(
            self.mccs_master_device_name, AdapterType.MCCS_CONTROLLER
        ).State()
        self.logger.info(
            "MCCS Controller Device State: %s", self.get_device().state
        )

        def check_device_state():
            """Check device state"""
            command_allowed_dev_states = {
                "On": [
                    DevState.OFF,
                    DevState.STANDBY,
                    DevState.UNKNOWN,
                ],
                "Off": [
                    DevState.ON,
                    DevState.STANDBY,
                ],
                "Standby": [
                    DevState.ON,
                    DevState.OFF,
                ],
                "AssignResources": [DevState.ON],
                "ReleaseAllResources": [DevState.ON],
            }

            if command_name in command_allowed_dev_states:
                if (
                    self.get_device().state
                    not in command_allowed_dev_states[command_name]
                ):
                    return False
            return True

        return check_device_state
