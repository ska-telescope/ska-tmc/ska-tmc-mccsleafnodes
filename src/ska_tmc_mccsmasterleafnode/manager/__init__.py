"""Init file for MCCSMasterLeafNode Manager"""

from ska_tmc_mccsmasterleafnode.manager.component_manager import (
    MccsMLNComponentManager,
)

__all__ = ["MccsMLNComponentManager"]
