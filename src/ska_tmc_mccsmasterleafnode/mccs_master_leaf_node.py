"""
This module uses the SKA Control Model for health state definitions and
SKA Tango Base for the base device class.
"""

from typing import List, Tuple, Union

from ska_control_model import AdminMode
from ska_tango_base.base.base_device import SKABaseDevice
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tango_base.control_model import HealthState
from ska_tmc_common.enum import LivelinessProbeType
from ska_tmc_common.exceptions import CommandNotAllowed, DeviceUnresponsive
from ska_tmc_common.v1.tmc_base_leaf_device import TMCBaseLeafDevice
from tango import AttrWriteType
from tango.server import attribute, command, device_property, run

from ska_tmc_mccsmasterleafnode import release
from ska_tmc_mccsmasterleafnode.commands.set_controller_admin_mode import (
    SetAdminMode,
)
from ska_tmc_mccsmasterleafnode.manager import MccsMLNComponentManager


class LowTmcLeafNodeMccs(TMCBaseLeafDevice):
    """
    LowTmcLeafNodeMccs class acts as an MCCS contact point for the Master Node
      and also to monitor and issue commands to the MCCS Master.
    """

    # pylint: disable= protected-access
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._issubsystemavailable: bool = False
        self.logger.debug("LowTmcLeafNodeMccs initialized.")

    # -----------------
    # Device Properties
    # -----------------

    MccsMasterFQDN = device_property(
        dtype="str",
        doc="FQDN of the MCCS Master Tango Device Server.",
    )
    CommandTimeOut = device_property(dtype="DevFloat", default_value=15)

    # -----------------
    # Attributes
    # -----------------
    mccsControllerAdminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ,
    )
    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)

    isSubsystemAvailable = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc="Indicates whether the MCCS subsystem is available.",
    )

    mccsMasterDevName = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        doc="Name of the MCCS Master device.",
    )

    class InitCommand(SKABaseDevice.InitCommand):
        """
        A class for LowTmcLeafNodeMccs's init_device() method.
        """

        # pylint: disable= arguments-differ
        def do(self) -> Tuple[ResultCode, str]:
            """
            Initializes the attributes and properties of the LowTmcLeafNodeMccs

            :return: A tuple containing a return code and a string message
              indicating status.
            :rtype: (ResultCode, str)
            """
            super().do()
            device = self._device

            device._build_state = (
                f"{release.name},{release.version},{release.description}"
            )
            device._health_state = HealthState.OK
            device._version_id = release.version
            device._admin_mode = AdminMode.ONLINE
            device._mccs_master_leaf_node_admin_mode = AdminMode.ONLINE
            device._is_admin_mode_enabled = True
            device._issubsystemavailable = False
            device.op_state_model.perform_action("component_on")
            for attribute_name in [
                "healthState",
                "isSubsystemAvailable",
                "mccsControllerAdminMode",
                "isAdminModeEnabled",
            ]:
                device.set_change_event(attribute_name, True, False)
                device.set_archive_event(attribute_name, True)
            return (
                ResultCode.OK,
                "LowTmcLeafNodeMccs initialized successfully.",
            )

    def always_executed_hook(self):
        pass

    def delete_device(self):
        """Clean up resources when the device is deleted."""
        if hasattr(self, "component_manager"):
            self.component_manager.stop_event_receiver()
            self.component_manager.stop_liveliness_probe()
            self.logger.info("Stopped event receiver and liveliness probe.")

    # ------------------
    # Attributes methods
    # ------------------

    def update_availablity_callback(self, availablity: bool) -> None:
        """Change event callback for isSubsystemAvailable."""
        if availablity != self._issubsystemavailable:
            self._issubsystemavailable = availablity
            self.push_change_archive_events(
                "isSubsystemAvailable", self._issubsystemavailable
            )
            self.logger.debug(
                "isSubsystemAvailable updated to %s.",
                self._issubsystemavailable,
            )

    # pylint: disable= attribute-defined-outside-init
    def update_admin_mode_callback(self, admin_mode: int):
        """Update MCCSMLNAdminMode attribute callback"""
        self._mccs_master_leaf_node_admin_mode = admin_mode
        try:
            self.push_change_archive_events(
                "mccsControllerAdminMode",
                self._mccs_master_leaf_node_admin_mode,
            )
            self.logger.info(
                "Successfully updated and pushed mccsControllerAdminMode "
                "attribute value to: %s",
                admin_mode,
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for "
                + "mccsControllerAdminMode: %s",
                exception,
            )

    def read_isSubsystemAvailable(self) -> bool:
        """Returns the isSubsystemAvailable attribute."""
        return self._issubsystemavailable

    def read_mccsMasterDevName(self) -> str:
        """Returns the mccsMasterDevName attribute."""
        return self.component_manager.mccs_master_device_name

    def read_mccsControllerAdminMode(self) -> int:
        """Read method for mccsControllerAdminMode Attribute"""
        return self._mccs_master_leaf_node_admin_mode

    def read_isAdminModeEnabled(self) -> bool:
        """Return the isAdminModeEnabled attribute value"""
        return self.component_manager.is_admin_mode_enabled

    def write_isAdminModeEnabled(self, value: bool):
        """Set the value of isAdminModeEnabled attribute"""
        self.component_manager.is_admin_mode_enabled = value

    def write_mccsMasterDevName(self, value: str) -> None:
        """Sets the mccsMasterDevName attribute."""
        self.component_manager.mccs_master_device_name = value
        self.logger.debug("mccsMasterDevName set to %s", value)

    def is_Off_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """
        Checks whether the Off command is allowed to be run in the current
          device state.

        :return: True if the Off command is allowed to be run in the current
          device state.
        :rtype: bool, CommandNotAllowed, DeviceUnresponsive
        """
        return self.component_manager.is_command_allowed("Off")

    @command(dtype_out="DevVarLongStringArray")
    def Off(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Off command on the MCCS Controller.

        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Invoking Off on Mccsmasterleafnode.")
        handler = self.get_command_object("Off")
        return_code, unique_id = handler()
        self.logger.debug(
            "Off command result: %s, ID: %s.", return_code, unique_id
        )
        return ([return_code], [str(unique_id)])

    def is_On_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """
        Checks whether the On command is allowed to be run in the current
          device state.

        :return: True if the On command is allowed to be run in the current
          device state.
        :rtype: bool, CommandNotAllowed, DeviceUnresponsive
        """
        return self.component_manager.is_command_allowed("On")

    @command(dtype_out="DevVarLongStringArray")
    def On(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the On command on the MCCS Controller.

        :return: A tuple containing a list of result codes and a list of
         unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Invoking On on Mccsmasterleafnode.")
        handler = self.get_command_object("On")
        return_code, unique_id = handler()
        self.logger.debug(
            "On command result: %s, ID: %s.", return_code, unique_id
        )
        return ([return_code], [str(unique_id)])

    def is_AssignResources_allowed(self) -> bool:
        """
        Checks whether the AssignResources command is allowed to be run in the
         current device state.

        :return: True if the AssignResources command is allowed to be run in
         the current device state.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("AssignResources")

    @command(
        dtype_in="str",
        doc_in=(
            "JSON-formatted string containing the subarray ID and "
            "resources to be allocated."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out=(
            "A tuple containing a list of result codes and a list of "
            "unique IDs."
        ),
    )
    def AssignResources(
        self, argin: str
    ) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the AssignResources command on the MCCS Controller.

        :param argin: JSON-formatted string containing an integer subarray ID
          and resources to be allocated.
        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]

        :example:

            >>> proxy = tango.DeviceProxy("low-tmc/leaf-node-mccs/0")
            >>> proxy.AssignResources(
                    json.dumps(
                    {
                        "interface": "https://schema.skao.int/
                        ska-low-mccs-controller-allocate/3.0",
                        "subarray_id": 1,
                            "subarray_beams": [
                            {
                                "subarray_beam_id": 1,
                                "apertures": [
                                {
                                    "station_id": 1,
                                    "aperture_id": "AP001.01"
                                },
                                {
                                    "station_id": 1,
                                    "aperture_id": "AP001.02"
                                },
                                {
                                    "station_id": 2,
                                    "aperture_id": "AP002.01"
                                },
                                {
                                    "station_id": 2,
                                    "aperture_id": "AP002.02"
                                },
                                {
                                    "station_id": 3,
                                    "aperture_id": "AP003.01"
                                }
                                ],
                                "number_of_channels": 32
                            }
                        ]
                        }
                    )
        """
        self.logger.info(
            "Invoking AssignResources on Mccsmasterleafnode with input: %s",
            argin,
        )
        handler = self.get_command_object("AssignResources")
        result_code, unique_id = handler(argin)
        self.logger.debug(
            "AssignResources command result: %s, ID: %s.",
            result_code,
            unique_id,
        )
        return [result_code], [unique_id]

    def is_ReleaseAllResources_allowed(self) -> bool:
        """
        Checks whether the ReleaseAllResources command is allowed to be run
          in the current device state.

        :return: True if the ReleaseAllResources command is allowed to be run
          in the current device state.
        :rtype: bool
        """
        return self.component_manager.is_command_allowed("ReleaseAllResources")

    @command(
        dtype_in="str",
        doc_in=(
            "JSON-formatted string containing the subarray ID to release "
            "resources from."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out=(
            "A tuple containing a list of result codes and a list of "
            "unique IDs."
        ),
    )
    def ReleaseAllResources(
        self, argin: str
    ) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the ReleaseAllResources command on the MCCS Controller.

        :param argin: JSON-formatted string containing the subarray ID to
          release resources from.
        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info(
            "Invoking ReleaseAllResources on Mccsmasterleafnode "
            "with input: %s",
            argin,
        )
        handler = self.get_command_object("ReleaseAllResources")
        result_code, unique_id = handler(argin)
        self.logger.debug(
            "ReleaseAllResources command result: %s, ID: %s.",
            result_code,
            unique_id,
        )
        return ([result_code], [str(unique_id)])

    def is_Standby_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """
        Checks whether the Standby command is allowed to be run in the current
          device state.

        :return: True if the Standby command is allowed to be run in the
          current device state.
        :rtype: bool, CommandNotAllowed, DeviceUnresponsive
        """
        return self.component_manager.is_command_allowed("Standby")

    @command(dtype_out="DevVarLongStringArray")
    def Standby(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the Standby command on the MCCS Controller.

        :return: A tuple containing a list of result codes and a list of
          unique IDs.
        :rtype: Tuple[List[ResultCode], List[str]]
        """
        self.logger.info("Invoking Standby on Mccsmasterleafnode.")
        handler = self.get_command_object("Standby")
        return_code, unique_id = handler()
        self.logger.debug(
            "Standby command result: %s, ID: %s.", return_code, unique_id
        )
        return ([return_code], [str(unique_id)])

    @command(
        dtype_in=AdminMode,
        doc_in="The adminMode in enum format",
        dtype_out="DevVarLongStringArray",
    )
    def SetAdminMode(self, argin: AdminMode):
        """
        This command sets the adminMode command on MCCS Controller.
        """
        handler = self.get_command_object("SetAdminMode")
        result_code, message = handler(argin)
        self.logger.debug(
            "SetAdminMode command result: %s, Message: %s.",
            result_code,
            message,
        )
        return [result_code], [message]

    def create_component_manager(self):
        """Returns Mccs Master Leaf Node component manager object"""
        cm = MccsMLNComponentManager(
            self.MccsMasterFQDN,
            logger=self.logger,
            _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
            _event_receiver=True,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            command_timeout=self.CommandTimeOut,
            _update_availablity_callback=self.update_availablity_callback,
            _update_admin_mode_callback=self.update_admin_mode_callback,
        )
        cm.mccs_master_device_name = self.MccsMasterFQDN or ""
        return cm

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        for command_name, method_name in [
            ("AssignResources", "assign_resources"),
            ("ReleaseAllResources", "release_resources"),
            ("On", "on_command"),
            ("Off", "off_command"),
            ("Standby", "standby_command"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=self.logger,
                ),
            )
        self.register_command_object(
            "SetAdminMode",
            SetAdminMode(self.component_manager, self.logger),
        )


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """
    Runs the LowTmcLeafNodeMccs.
    :param args: Arguments internal to TANGO

    :param kwargs: Arguments internal to TANGO

    :return: LowTmcLeafNodeMccs TANGO object.
    """
    return run((LowTmcLeafNodeMccs,), args=args, **kwargs)


if __name__ == "__main__":
    main()
