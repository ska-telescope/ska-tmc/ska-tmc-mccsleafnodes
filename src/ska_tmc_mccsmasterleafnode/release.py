# -*- coding: utf-8 -*-
#
# This file is part of the LowTmcLeafNodeMccs project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
# pylint: disable=redefined-builtin

"""Release information for Python Package"""

name = """ska_tmc_mccsmasterleafnode"""
version = "0.6.2"
version_info = version.split(".")
description = """ The primary responsibility of the MCCS Master
                  Leaf node is to monitor the MCCS Controller
                  and issue control actions during an observation."""
author = "Team HIMALAYA, Team SAHYADRI"
author_email = "telmgt-internal@googlegroups.com"
license = """BSD-3-Clause"""
url = """https://www.skaobservatory.org"""
copyright = """"""
