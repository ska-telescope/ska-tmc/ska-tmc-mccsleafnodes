"""
Mccs Master Leaf Node Command module
"""
import time
from logging import Logger
from typing import Any, Optional, Tuple

from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType, TmcLeafNodeCommand
from tango import ConnectionFailed, DevFailed


# pylint: disable=abstract-method
# add adaptor factory
class MccsMLNCommand(TmcLeafNodeCommand):
    """
    A common command class for Mccs Master Leaf Node
    """

    def __init__(
        self,
        component_manager,
        logger: Logger,
    ):
        super().__init__(component_manager, logger)
        self.mccs_master_adapter = None

    def reject_command(self, message):
        """
        Command will get rejected with a given message .
        """
        self.logger.error(message)
        return TaskStatus.REJECTED, message

    def init_adapter(self) -> Tuple[ResultCode, str]:
        dev_name = self.component_manager.mccs_master_device_name
        adapter_timeout = self.component_manager.adapter_timeout
        elapsed_time = 0
        start_time = time.time()

        while (
            self.mccs_master_adapter is None and elapsed_time < adapter_timeout
        ):
            try:
                self.mccs_master_adapter = (
                    self.adapter_factory.get_or_create_adapter(
                        dev_name, AdapterType.MCCS_CONTROLLER
                    )
                )

            except ConnectionFailed as connection_failed:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    return (
                        ResultCode.FAILED,
                        f"Error in creating adapter for "
                        f"{dev_name}: {connection_failed}",
                    )
            except DevFailed as dev_failed:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    return (
                        ResultCode.FAILED,
                        f"Error in creating adapter for "
                        f"{dev_name}: {dev_failed}",
                    )
            except (AttributeError, ValueError, TypeError) as exception:
                return (
                    ResultCode.FAILED,
                    f"Error in creating adapter for "
                    f"{dev_name}: {exception}",
                )

        return ResultCode.OK, "Adapter initialisation is successful"

    def do_low(self, argin: Optional[Any] = None):
        """Abstract Method from TmcLeafNodeCommand is
            defined here but not utilized by this Class.

        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """

    def do(self, argin: Optional[Any] = None):
        """Abstract Method from TmcLeafNodeCommand is
            defined here but not utilized by this Class.

        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None."""

    def update_task_status(self, **kwargs):
        """Abstract Method from TmcLeafNodeCommand is
        defined here but not utilized by this Class."""

    def init_adapter_low(self):
        """
        Initialize the adapter for the low-level device.
        """
        self.init_adapter()

    def set_command_id(self, command_name: str):
        """
        Sets the command id for error propagation.

        :param command_name: Name of the command
        :type command_name: str
        """
        command_id = f"{time.time()}-{command_name}"
        self.logger.info(
            "Setting command id as %s for command: %s",
            command_id,
            command_name,
        )
        self.component_manager.command_id = command_id
        self.component_manager.command_result[
            self.component_manager.command_id
        ] = None
