"""
Off command class for LowTmcLeafNodeMccs.
"""
import threading
from logging import Logger
from typing import Any, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus

from ska_tmc_mccsmasterleafnode.commands.mccs_mln_command import MccsMLNCommand
from ska_tmc_mccsmasterleafnode.utils.constants import (
    MCCSMLN_RESULTCODE_FAILED,
    RESULTCODE_OK,
)


# pylint: disable=abstract-method
class Off(MccsMLNCommand):
    """
    A class for LowTmcLeafNodeMccs's Off() command.

    Off command on LowTmcLeafNodeMccs enables the telescope to perform
    further operations and observations.
    It Invokes Off command on Mccs Controller device.

    """

    def off(
        self,
        logger: Logger,
        task_callback: TaskCallbackType,
        # pylint: disable= unused-argument
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """A method to invoke the Off command.
        It sets the task_callback status according to command progress.

        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state
        :type task_callback:  TaskCallbackType, optional
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """
        task_callback(status=TaskStatus.IN_PROGRESS)
        result_code, message = self.do()
        if result_code in MCCSMLN_RESULTCODE_FAILED:
            result = (result_code, message)
            task_callback(
                status=TaskStatus.COMPLETED,
                result=result,
                exception=message,
            )
        else:
            result = RESULTCODE_OK
            task_callback(
                status=TaskStatus.COMPLETED,
                result=result,
            )

        self.logger.info("Off command task status updated to: %s ", result)

    def do(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke Off command on Mccs Controller.

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Controller", self.mccs_master_adapter, "Off"
        )

        self.logger.info(
            "%s Off command -> ResultCode: %s and Message: %s",
            self.mccs_master_adapter.dev_name,
            result_code,
            unique_id_or_message,
        )

        return result_code[0], unique_id_or_message[0]
