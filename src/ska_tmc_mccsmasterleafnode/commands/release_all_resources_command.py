"""
ReleaseAllResources command class for MCCS Master Leaf Node.
"""

import json
import time
from json import JSONDecodeError
from logging import Logger
from typing import Callable, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccsmasterleafnode.commands.mccs_mln_command import MccsMLNCommand
from ska_tmc_mccsmasterleafnode.utils.constants import MCCS_RELEASE_VERSION


# pylint: disable=abstract-method
class ReleaseAllResources(MccsMLNCommand):
    """
    A class to send Release command to MCCS Controller.
    """

    def __init__(self, component_manager, logger: Logger) -> None:
        super().__init__(component_manager, logger)
        self.component_manager = component_manager
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType
        self.unique_id: str = ""

    # pylint: disable=unused-argument
    @timeout_tracker
    @error_propagation_tracker("get_lrcr_result", [ResultCode.OK])
    def release_all_resources(self, argin: str) -> Tuple[ResultCode, str]:
        """This is a long running method for ReleaseAllResources command, it
        executes do hook, invokes Release command on MCCS Master Leaf Node.

        :param argi: Input JSON string
        :type argin: str
        :param task_callback: Update task state, defaults to None
        :type task_callback:  TaskCallbackType
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event
        """
        return self.do(argin)

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        try:
            result = kwargs.get("result")
            status = kwargs.get("status", TaskStatus.COMPLETED)
            message = kwargs.get("exception")
            if result[0] == ResultCode.OK:
                self.component_manager.command_in_progress = None
                self.task_callback(result=result, status=status)
            elif status == TaskStatus.ABORTED:
                self.task_callback(status=status)
            else:
                self.task_callback(
                    result=result,
                    status=status,
                    exception=message,
                )
                self.component_manager.command_in_progress = None

            self.component_manager.command_id = ""
        except (KeyError, ValueError) as exception:
            self.logger.exception(
                "Exception occured while updating task status %s"
                "Received task_status: %s",
                exception,
                kwargs,
            )
        self.logger.info("Relase command task status updated to: %s", kwargs)

    def do(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Method to invoke Release command.

        :param argin: The string in JSON format.
        :return:
            A tuple containing a return code and
            "" as a string on successful release all resources.
            or
            Exception Message on failure / Rejection

        rtype:
            Tuple[ResultCode, str]
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            json_argument = json.loads(argin)
            json_argument["interface"] = MCCS_RELEASE_VERSION

        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of release all resources command is failed with "
                + "reason:: JSON parsing failed with exception:: %s",
                json_error,
            )

            return (
                ResultCode.FAILED,
                f"Exception occurred while parsing the JSON: {json_error}",
            )

        result_code, unique_id_or_message = self.call_adapter_method(
            "MCCS Controller",
            self.mccs_master_adapter,
            "Release",
            argin=argin,
        )
        if result_code[0] in [ResultCode.FAILED, ResultCode.REJECTED]:
            self.logger.error(
                "Release command failed or was rejected: %s",
                unique_id_or_message[0],
            )
            return ResultCode.FAILED, unique_id_or_message[0]

        if result_code[0] in [ResultCode.QUEUED, ResultCode.OK]:
            self.component_manager.command_mapping[
                unique_id_or_message[0]
            ] = self.component_manager.command_id
            self.logger.info(
                "%s ReleaseAllResources command -> "
                "ResultCode: %s and Message: %s",
                self.mccs_master_adapter.dev_name,
                result_code,
                unique_id_or_message,
            )
        return ResultCode.STARTED, ""
