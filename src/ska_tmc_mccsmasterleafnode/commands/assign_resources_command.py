"""
AssignResources command class for MCCS Master Leaf Node.
"""
import json
import time
from json import JSONDecodeError
from logging import Logger
from typing import Callable, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_mccsmasterleafnode.commands.mccs_mln_command import MccsMLNCommand
from ska_tmc_mccsmasterleafnode.utils.constants import MCCS_ASSIGN_VERSION


# pylint: disable=abstract-method
class AssignResources(MccsMLNCommand):
    """
    A class for MCCS Master LeafNode's AssignResources() command.
    Assign resources command will be sent to  MCCS Controller.
    """

    def __init__(self, component_manager, logger: Logger):
        super().__init__(component_manager, logger)
        self.component_manager = component_manager
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType
        self.unique_id: str = ""

    @timeout_tracker
    @error_propagation_tracker("get_lrcr_result", [ResultCode.OK])
    def assign_resources(self, argin: str) -> Tuple[ResultCode, str]:
        """
        This is a long running method for the AssignResources command on
        MCCS Master Leaf Node. It executes the `do` hook and invokes the
        Allocate command on the MCCS Controller.

        :param argin: Input JSON string.
        :type argin: str
        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]
        """
        return self.do(argin)

    def update_task_status(self, **kwargs) -> None:
        """
        Method to update task status with result code and exception
        message if any.

        :param kwargs: Keyword arguments containing task result details.
        """

        result = kwargs["result"]

        if result[0] == ResultCode.FAILED:
            self.task_callback(
                status=TaskStatus.COMPLETED,
                result=result,
                exception=kwargs["exception"],
            )
            self.logger.error(
                "AssignResources task failed with exception: %s",
                kwargs["exception"],
            )
        else:
            self.task_callback(status=TaskStatus.COMPLETED, result=result)
            self.logger.info(
                "AssignResources task completed with result: %s", result
            )

        self.component_manager.command_in_progress = ""
        self.component_manager.clear_command_mapping(self.unique_id)
        self.logger.debug(
            "Command progress and mapping cleared for unique_id: %s",
            self.unique_id,
        )

    def do(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Method to invoke the Allocate command on the MCCS Controller.

        :param argin: The string in JSON format.
            The JSON contains the following values:
            - resources: List of resources to be assigned.
        :type argin: str
        :return: A tuple containing the result code and a message or unique ID.
        :rtype: Tuple[ResultCode, str]
        """
        self.logger.info("Starting AssignResources command execution.")
        result_code, message = self.init_adapter()

        if result_code == ResultCode.FAILED:
            self.logger.debug("Initialization of adapter failed: %s", message)
            return result_code, message

        try:
            json_argument = json.loads(argin)
            json_argument["interface"] = MCCS_ASSIGN_VERSION
        except JSONDecodeError as json_error:
            self.logger.exception(
                "JSON parsing failed with exception: %s", json_error
            )
            return (
                ResultCode.FAILED,
                f"Exception occurred while parsing the JSON: {json_error}",
            )

        return_code, message_or_unique_id = self.call_adapter_method(
            "MCCS Controller",
            self.mccs_master_adapter,
            "Allocate",
            argin=json.dumps(json_argument),
        )

        if return_code[0] in [ResultCode.FAILED, ResultCode.REJECTED]:
            self.logger.error(
                "Allocate command failed or was rejected: %s",
                message_or_unique_id[0],
            )
            return ResultCode.FAILED, message_or_unique_id[0]

        if return_code[0] in [ResultCode.QUEUED, ResultCode.OK]:
            self.component_manager.command_mapping[
                message_or_unique_id[0]
            ] = self.component_manager.command_id

            self.logger.info(
                "Allocate command queued for execution with "
                "unique_id: %s, "
                "device name: %s, "
                "result code: %s, message: %s",
                message_or_unique_id[0],
                self.mccs_master_adapter.dev_name,
                result_code,
                message,
            )

        return ResultCode.STARTED, ""
