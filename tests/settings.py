"""Common settings for testing of MCCS Leaf Nodes

"""
import json
import logging
import time
from typing import List, Union

import tango
from ska_control_model import ObsState
from ska_tango_base.commands import ResultCode
from ska_tmc_common import DevFactory, FaultType, LivelinessProbeType
from tango import DeviceProxy, DevState

from ska_tmc_mccsmasterleafnode.manager.component_manager import (
    MccsMLNComponentManager,
)
from ska_tmc_mccssubarrayleafnode.manager.component_manager import (
    MccsSLNComponentManager,
)

logger = logging.getLogger(__name__)

SLEEP_TIME = 0.5
TIMEOUT = 100

MCCS_MASTER_LN_DEVICE = "low-tmc/leaf-node-mccs/0"
MCCS_CONTROLLER_DEVICE = "low-mccs/control/control"
MCCS_SUBARRAY_DEVICE = "low-mccs/subarray/01"
MCCS_SUBARRAY_LEAF_NODE_DEVICE = "low-tmc/subarray-leaf-node-mccs/01"
RESULTCODE_OK = '[0, "Command Completed"]'
TIMEOUT_OCCURRED = json.dumps(
    [ResultCode.FAILED, "Timeout has occurred, command failed"]
)


def count_faulty_devices(cm):
    """Method for counting faulty devices."""
    result = 0
    logger.debug("cm.checked_devices: %s", cm.checked_devices)
    for dev_info in cm.checked_devices:
        if dev_info.unresponsive:
            result += 1
    return result


def create_cm_mccsmln(device):
    """Returns component manager for LowTmcLeafNodeMccs."""
    cm = MccsMLNComponentManager(
        device,
        logger=logger,
        _liveliness_probe=LivelinessProbeType.NONE,
        _event_receiver=True,
    )
    cm.get_device().state = DevState.DISABLE
    return cm


def get_mccsmln_command_obj(command_class, device):
    """Returns component manager and command class object for
    MCCS Master Leaf Node"""
    cm = create_cm_mccsmln(device)
    command_obj = command_class(cm, logger=logger)
    return cm, command_obj


# pylint: disable= protected-access
def event_remover(change_event_callbacks, attributes: List[str]) -> None:
    """Removes residual events from the queue."""
    for attribute in attributes:
        try:
            iterable = change_event_callbacks._mock_consumer_group._views[
                attribute
            ]._iterable
            for node in iterable:
                logger.info("Payload is: %s", repr(node.payload))
                node.drop()
        except KeyError:
            pass


def create_cm_mccssln() -> MccsSLNComponentManager:
    """Creates and returns component manager instance for the
    MCCSSubarrayLeafNode.
    """
    cm = MccsSLNComponentManager(
        MCCS_SUBARRAY_DEVICE,
        MCCS_CONTROLLER_DEVICE,
        logger=logger,
        _liveliness_probe=LivelinessProbeType.NONE,
        _event_receiver=True,
    )
    return cm


def tear_down_mccsmln(mccsmln_node, group_callback):
    """Tear down method for
    Mccs master leaf node"""

    MCCSMLN_LRCR_ID = mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )

    logger.info("Inside tear_down_mccsmln teardown")
    result, unique_id = mccsmln_node.Off()

    logger.info("Command ID: %s Returned result: %s", unique_id, result)
    assert result[0] == ResultCode.QUEUED

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=20
    )
    mccsmln_node.unsubscribe_event(MCCSMLN_LRCR_ID)


TIMEOUT_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.FAILED_RESULT,
        "error_message": "Timeout has occurred, command failed",
        "result": ResultCode.FAILED,
    }
)

# Master MCCS Leaf Node does not have any obs-state
STUCK_IN_INTERMEDIATE_STATE_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Device stuck in intermediate state",
        "result": ResultCode.FAILED,
    }
)


ERROR_PROPAGATION_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.LONG_RUNNING_EXCEPTION,
        "error_message": "Exception occured, command failed.",
        "result": ResultCode.FAILED,
    }
)

RESET_DEFECT = json.dumps(
    {
        "enabled": False,
        "fault_type": FaultType.FAILED_RESULT,
        "error_message": "Default exception.",
        "result": ResultCode.FAILED,
    }
)


TIMEOUT_EXCEPTION = "Timeout has occurred, command failed"

COMMAND_NOT_ALLOWED_AFTER_QUEUING = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.COMMAND_NOT_ALLOWED_AFTER_QUEUING,
        "error_message": "Command is not allowed",
        "result": ResultCode.QUEUED,
    }
)


def wait_for_mccs_obsstate(
    cm: MccsSLNComponentManager, obs_state: ObsState
) -> bool:
    """Wait for MCCS SLN to reflect the obsState change in MCCS Subarray.

    :param cm: Component manager instance for MCCS SLN device.
    :cm dtype: MccsSLNComponentManager class instance.
    :param obs_state: Expected ObsState
    :obs_state dtype: ObsState

    :rtype: bool
    """
    start_time = time.time()
    while cm.get_obs_state() != obs_state:
        time.sleep(1)
        if time.time() - start_time >= TIMEOUT:
            logger.debug(
                "Current obsState after timeout is: %s", cm.get_obs_state()
            )
            return False
    return True


def simulate_obs_state_event_for_mccs_subarray(
    cm: Union[MccsMLNComponentManager, MccsSLNComponentManager],
    obs_states: List[ObsState],
) -> None:
    """Simulates an ObsState event from MCCS Subarray on the given component
    manager.
    """
    for obs_state in obs_states:
        cm.update_device_obs_state(MCCS_SUBARRAY_DEVICE, obs_state)


def set_mccs_subarray_obs_state(obs_state: ObsState) -> None:
    """Sets the ObsState for the MCCS Subarray Device to the given ObsState.

    :param obs_state: Expected ObsState
    :obs_state dtype: ObsState

    :rtype: None
    """
    dev_factory = DevFactory()
    mccs_subarray = dev_factory.get_device(MCCS_SUBARRAY_DEVICE)
    mccs_subarray.SetDirectObsState(obs_state)


def wait_for_attribute_value(
    device: DeviceProxy, attribute_name: str, value: str = "[]"
) -> bool:
    """Waits for attribute value to change on the given device."""
    start_time = time.time()
    while device.read_attribute(attribute_name).value != value:
        time.sleep(0.5)
        if time.time() - start_time >= TIMEOUT:
            return False
    return True
