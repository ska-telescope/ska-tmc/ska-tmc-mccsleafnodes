import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceUnresponsive,
    HelperAdapterFactory,
)

from ska_tmc_mccssubarrayleafnode.commands import Abort
from tests.settings import (
    MCCS_SUBARRAY_DEVICE,
    create_cm_mccssln,
    logger,
    set_mccs_subarray_obs_state,
    wait_for_mccs_obsstate,
)


@pytest.mark.mccssln
def test_abort_command_in_resourcing(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command completion

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.RESOURCING)
    wait_for_mccs_obsstate(cm, ObsState.RESOURCING)
    cm.is_command_allowed("Abort")
    logger.info(f"Abort command is allowed in {ObsState.RESOURCING}.")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.mccssln
def test_abort_command_in_idle(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command completion

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    cm.is_command_allowed("Abort")
    logger.info(f"Abort command is allowed in {ObsState.IDLE}.")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.mccssln
def test_abort_command_in_configuring(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command completion

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.CONFIGURING)
    wait_for_mccs_obsstate(cm, ObsState.CONFIGURING)
    cm.is_command_allowed("Abort")
    logger.info(f"Abort command is allowed in {ObsState.CONFIGURING}.")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.mccssln
def test_abort_command_in_ready(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command completion

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    cm.is_command_allowed("Abort")
    logger.info(f"Abort command is allowed in {ObsState.READY}.")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.mccssln
def test_abort_command_in_scanning(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command completion

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.SCANNING)
    wait_for_mccs_obsstate(cm, ObsState.SCANNING)
    cm.is_command_allowed("Abort")
    logger.info(f"Abort command is allowed in {ObsState.SCANNING}.")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.mccssln
def test_abort_command_fail_subarray(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Abort command failure when the subarray encounters an
    exception.

    Args:
        tango_context: The Tango context for the test case.
        obsstate: Verify abort command in allowed obssate
    """
    cm = create_cm_mccssln()
    adapter_factory = HelperAdapterFactory()
    attrs = {"Abort.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=subarrayMock,
    )
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    abort = Abort(cm, logger)
    abort.adapter_factory = adapter_factory
    assert cm.is_command_allowed("Abort")
    (result_code, message) = abort.do()
    assert result_code == ResultCode.FAILED
    assert MCCS_SUBARRAY_DEVICE in message


@pytest.mark.mccssln
def test_abort_command_fail_check_allowed_with_invalid_obsState(
    tango_context,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Abort command failure when the device's ObsState is invalid

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.EMPTY)
    wait_for_mccs_obsstate(cm, ObsState.EMPTY)
    with pytest.raises(
        CommandNotAllowed,
        match=r"Abort command on this device is not allowed*",
    ):
        cm.is_command_allowed("Abort")


@pytest.mark.mccssln
def test_telescope_abort_command_fail_check_allowed_with_device_unresponsive(
    tango_context,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Abort command failure when the device is unresponsive.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        DeviceUnresponsive,
    ):
        cm.get_device().update_unresponsive(True)
        cm.is_command_allowed("Abort")
