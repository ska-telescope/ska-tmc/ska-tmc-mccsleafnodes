"""Test cases to verify
Mccs Subarray Leaf Node Configure command"""
import json
import threading

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DevFactory,
    DeviceUnresponsive,
    HelperAdapterFactory,
)

from ska_tmc_mccssubarrayleafnode.commands import Configure
from tests.settings import (
    MCCS_SUBARRAY_DEVICE,
    create_cm_mccssln,
    logger,
    set_mccs_subarray_obs_state,
    wait_for_mccs_obsstate,
)


@pytest.mark.mccssln
def test_configure_command_completed(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for successful completion of the Configure command.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    dev_factory = DevFactory()
    mccs_subarray = dev_factory.get_device(MCCS_SUBARRAY_DEVICE)
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    configure_input_str = json_factory("mccssln_configure_json")
    mccs_subarray.SetDelayInfo(json.dumps({"CONFIGURE": 20}))
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )
    mccs_subarray.ResetDelayInfo()


@pytest.mark.mccssln
def test_configure_command_fail_subarray(
    tango_context,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Configure command failure when the subarray encounters an
    exception.

    Args:
        tango_context: The Tango context for the test case.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    adapter_factory = HelperAdapterFactory()
    attrs = {"Configure.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=subarrayMock,
    )
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    configure = Configure(cm, logger)
    configure.adapter_factory = adapter_factory
    configure_input_str = json_factory("mccssln_configure_json")
    assert cm.is_command_allowed("Configure")
    (result_code, message) = configure.do(configure_input_str)
    assert result_code == ResultCode.FAILED
    assert MCCS_SUBARRAY_DEVICE in message


@pytest.mark.mccssln
def test_configure_command_fail_check_allowed_with_invalid_obsState(
    tango_context,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Configure command failure when the device's ObsState is
    invalid.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        CommandNotAllowed,
        match=r"Configure command on this device is not allowed*",
    ):
        cm.is_command_allowed("Configure")


@pytest.mark.mccssln
def test_telescope_configure_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    tango_context,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Configure command failure when the device is unresponsive.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        DeviceUnresponsive,
        match="",
    ):
        cm.get_device().update_unresponsive(True)
        cm.is_command_allowed("Configure")


@pytest.mark.mccssln
def test_configure_command_timeout(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Configure command failure due to command timeout.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """

    cm = create_cm_mccssln()
    attrs = {
        "Configure.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Time out occurred for Configure command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    configure_input_str = json_factory("mccssln_configure_json")
    configure_command = Configure(cm, logger)
    configure_command.adapter_factory = adapter_factory
    cm.is_command_allowed("Configure")
    configure_command.invoke_configure(
        argin=configure_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Configure command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Time out occurred for Configure command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the Configure command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Time out occurred for Configure command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )


@pytest.mark.mccssln
def test_configure_command_rejected(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case of the Configure command Rejected from base class.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.EMPTY)
    wait_for_mccs_obsstate(cm, ObsState.EMPTY)
    configure_input_str = json_factory("mccssln_configure_json")
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        status=TaskStatus.REJECTED,
        result=(ResultCode.NOT_ALLOWED, "Command is not allowed"),
    )


@pytest.mark.mccssln
def test_configure_command_error_propagation(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Configure command failure due to exception occured.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """

    cm = create_cm_mccssln()
    attrs = {
        "Configure.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Exception occurred for Configure command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    set_mccs_subarray_obs_state(ObsState.IDLE)
    wait_for_mccs_obsstate(cm, ObsState.IDLE)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    configure_input_str = json_factory("mccssln_configure_json")
    configure_command = Configure(cm, logger)
    configure_command.adapter_factory = adapter_factory
    cm.is_command_allowed("Configure")
    configure_command.invoke_configure(
        argin=configure_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Configure command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Exception occurred for Configure command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the Configure command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Exception occurred for Configure command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )
