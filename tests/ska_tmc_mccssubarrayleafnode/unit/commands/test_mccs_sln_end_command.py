"""Test cases to verify
Mccs Subarray Leaf Node End command"""
import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceUnresponsive,
    HelperAdapterFactory,
)

from ska_tmc_mccssubarrayleafnode.commands import End
from tests.settings import (
    MCCS_SUBARRAY_DEVICE,
    create_cm_mccssln,
    logger,
    set_mccs_subarray_obs_state,
    simulate_obs_state_event_for_mccs_subarray,
    wait_for_mccs_obsstate,
)


@pytest.mark.mccssln
def test_end_command_completed(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for successful completion of the End command.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_event_for_mccs_subarray(cm, [ObsState.IDLE])
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.mccssln
def test_end_command_fail_subarray(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for End command failure when the subarray encounters an
    exception.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    adapter_factory = HelperAdapterFactory()

    # include exception in End command
    attrs = {"GoToIdle.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE, AdapterType.SUBARRAY, proxy=subarrayMock
    )
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    end_command = End(cm, logger)
    end_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("End")
    (result_code, message) = end_command.do()
    assert result_code == ResultCode.FAILED
    assert MCCS_SUBARRAY_DEVICE in message


@pytest.mark.mccssln
def test_end_command_fail_check_allowed_with_invalid_obsState(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for End command failure when the device's ObsState is invalid

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        CommandNotAllowed,
        match=r"End command on this device is not allowed*",
    ):
        cm.is_command_allowed("End")


@pytest.mark.mccssln
def test_end_command_fail_check_allowed_with_device_unresponsive(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for End command failure when the device is unresponsive.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        DeviceUnresponsive,
        match="",
    ):
        cm.get_device().update_unresponsive(True)
        cm.is_command_allowed("End")


@pytest.mark.mccssln
def test_end_command_timeout(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for End command failure due to command timeout.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    adapter_factory = HelperAdapterFactory()
    attrs = {
        "End.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Time out occurred for End command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    end = End(cm, logger)
    end.adapter_factory = adapter_factory
    cm.is_command_allowed("End")
    end.end(
        task_callback=task_callback, argin=None, task_abort_event=mock.Mock()
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the End command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Time out occurred for End command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the End command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Time out occurred for End command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )


@pytest.mark.mccssln
def test_end_command_error_propagation(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for End command failure due to exception occured.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    adapter_factory = HelperAdapterFactory()
    attrs = {
        "End.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Exception occurred for End command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    end = End(cm, logger)
    end.adapter_factory = adapter_factory
    cm.is_command_allowed("End")
    end.end(
        task_callback=task_callback, argin=None, task_abort_event=mock.Mock()
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the End command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Exception occurred for End command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the End command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Exception occurred for End command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )
