"""Test cases to verify
Mccs Subarray Leaf Node Restart command"""


import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceUnresponsive,
    HelperAdapterFactory,
)

from ska_tmc_mccssubarrayleafnode.commands.restart_command import Restart
from tests.settings import (
    MCCS_CONTROLLER_DEVICE,
    create_cm_mccssln,
    logger,
    set_mccs_subarray_obs_state,
    wait_for_mccs_obsstate,
)


@pytest.mark.mccssln
def test_restart_command_completed(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for successful completion of the Restart command.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.ABORTED)
    wait_for_mccs_obsstate(cm, ObsState.ABORTED)
    cm.restart(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.COMPLETED, "result": ResultCode.OK}
    )


@pytest.mark.mccssln
def test_restart_command_failure_due_to_exception_in_mccs_controller(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """ "
    Test case for verifying the failure scenario of the Restart command
    when an exception occurs in the MCCS controller.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    adapter_factory = HelperAdapterFactory()

    # include exception in Restart command
    attrs = {"Restart.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_CONTROLLER_DEVICE, AdapterType.MCCS_CONTROLLER, proxy=subarrayMock
    )
    set_mccs_subarray_obs_state(ObsState.ABORTED)
    wait_for_mccs_obsstate(cm, ObsState.ABORTED)
    restart_command = Restart(cm, logger)
    restart_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("Restart")
    (result_code, message) = restart_command.do()
    assert result_code == ResultCode.FAILED
    assert (
        "The invocation of the RestartSubarray command is failed on MCCS Controller device"  # noqa: E501
        in message
    )


@pytest.mark.mccssln
def test_restart_command_fail_check_allowed_with_device_unresponsive(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Restart command failure when the  Mccs Controller device
    is unresponsive.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        DeviceUnresponsive,
        match="",
    ):
        cm.get_device().update_unresponsive(True)
        cm.is_command_allowed("Restart")


@pytest.mark.mccssln
def test_restart_command_fail_check_allowed_with_invalid_obsState(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Restart command failure when the Mccs Controller
    device's ObsState is invalid

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        CommandNotAllowed,
        match=r"Restart command on this device is not allowed*",
    ):
        cm.is_command_allowed("Restart")
