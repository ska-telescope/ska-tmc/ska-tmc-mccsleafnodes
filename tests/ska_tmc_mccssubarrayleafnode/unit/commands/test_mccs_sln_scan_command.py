"""Test cases to verify
Mccs Subarray Leaf Node Scan command"""

import threading

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceUnresponsive,
    HelperAdapterFactory,
)

from ska_tmc_mccssubarrayleafnode.commands import Scan
from tests.settings import (
    MCCS_SUBARRAY_DEVICE,
    create_cm_mccssln,
    logger,
    set_mccs_subarray_obs_state,
    simulate_obs_state_event_for_mccs_subarray,
    wait_for_mccs_obsstate,
)


@pytest.mark.mccssln
def test_scan_command_completed(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for successful completion of the Scan command.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    scan_input_str = json_factory("mccssln_scan_json")
    cm.scan(scan_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_event_for_mccs_subarray(cm, [ObsState.SCANNING])
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.mccssln
def test_scan_command_fail_subarray(
    tango_context, json_factory, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Scan command failure when the subarray encounters an
    exception.

    Args:
        tango_context: The Tango context for the test case.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    adapter_factory = HelperAdapterFactory()
    attrs = {"scan.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=subarrayMock,
    )
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    scan = Scan(cm, logger)
    scan.adapter_factory = adapter_factory
    scan_input_str = json_factory("mccssln_scan_json")
    assert cm.is_command_allowed("Scan")
    (result_code, message) = scan.do(scan_input_str)
    assert result_code == ResultCode.FAILED
    assert MCCS_SUBARRAY_DEVICE in message


@pytest.mark.mccssln
def test_scan_command_fail_check_allowed_with_invalid_obsState(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Scan command failure when the device's ObsState is invalid.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        CommandNotAllowed,
        match=r"Scan command on this device is not allowed*",
    ):
        cm.is_command_allowed("Scan")


@pytest.mark.mccssln
def test_telescope_scan_command_fail_check_allowed_with_device_unresponsive(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test case for Scan command failure when the device is unresponsive.

    Args:
        tango_context: The Tango context for the test case.
    """
    cm = create_cm_mccssln()
    with pytest.raises(
        DeviceUnresponsive,
        match="",
    ):
        cm.get_device().update_unresponsive(True)
        cm.is_command_allowed("Scan")


@pytest.mark.mccssln
def test_scan_command_rejected(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case of the Scan command Rejected from base class.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.EMPTY)
    wait_for_mccs_obsstate(cm, ObsState.EMPTY)
    scan_input_str = json_factory("mccssln_scan_json")
    cm.scan(scan_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        status=TaskStatus.REJECTED,
        result=(ResultCode.NOT_ALLOWED, "Command is not allowed"),
    )


@pytest.mark.mccssln
def test_scan_command_timeout(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Scan command failure due to command timeout.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    attrs = {
        "Scan.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Time out occurred for Scan command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    scan_command = Scan(cm, logger)
    scan_command.adapter_factory = adapter_factory
    cm.is_command_allowed("Scan")
    scan_input_str = json_factory("mccssln_scan_json")
    scan_command.scan(
        argin=scan_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Scan command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Time out occurred for Scan command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the Scan command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Time out occurred for Scan command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )


@pytest.mark.mccssln
def test_scan_command_error_propagation(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    """
    Test case for Scan command failure due to command timeout.

    Args:
        tango_context: The Tango context for the test case.
        task_callback: A callback function to handle task status.
        json_factory: A factory function to create JSON input for the test case
    """
    cm = create_cm_mccssln()
    set_mccs_subarray_obs_state(ObsState.READY)
    wait_for_mccs_obsstate(cm, ObsState.READY)
    attrs = {
        "Scan.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(
                "Exception occurred for Scan command on"
                + f" {MCCS_SUBARRAY_DEVICE}"
            )
        )
    }
    mccssamock = mock.Mock(**attrs)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_SUBARRAY_DEVICE,
        AdapterType.SUBARRAY,
        proxy=mccssamock,
    )
    scan_command = Scan(cm, logger)
    scan_command.adapter_factory = adapter_factory
    cm.is_command_allowed("Scan")
    scan_input_str = json_factory("mccssln_scan_json")
    scan_command.scan(
        argin=scan_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Scan command is failed "
            + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
            + "The following exception occurred -"
            + " Exception occurred for Scan command on"
            + f" {MCCS_SUBARRAY_DEVICE}.",
        ),
        exception="The invocation of the Scan command is failed "
        + f"on MCCS Subarray device {MCCS_SUBARRAY_DEVICE}.\n"
        + "The following exception occurred -"
        + " Exception occurred for Scan command on"
        + f" {MCCS_SUBARRAY_DEVICE}.",
    )
