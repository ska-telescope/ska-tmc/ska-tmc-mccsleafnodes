import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import AdminMode

from ska_tmc_mccssubarrayleafnode.commands.set_mccs_subarray_admin_mode import (  # noqa: E501
    SetAdminMode,
)
from tests.settings import create_cm_mccssln, logger


@pytest.mark.mccssln
def test_set_admin_mode_command_low(tango_context):
    """
    Test the successful
    execution of the SetAdminMode command.

    """
    cm = create_cm_mccssln()
    argin = AdminMode.ONLINE
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)
    assert result_code == ResultCode.OK
    assert message == "Command Completed"


@pytest.mark.mccsmln
def test_invalid_admin_mode_command_low(
    tango_context,
):
    """Test to set the adminMode on mccs Subarray"""
    cm = create_cm_mccssln()

    argin = 7  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.FAILED
    assert message == "Command Failed"


@pytest.mark.mccsmln
def test_feature_toggle_adminMode(tango_context):
    """Test to set the adminMode on mccs Subarray"""
    cm = create_cm_mccssln()

    cm.is_admin_mode_enabled = False
    argin = AdminMode.ONLINE  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.NOT_ALLOWED
    assert message == (
        "AdminMode functionality is disabled, "
        + "Device will function normally."
    )
