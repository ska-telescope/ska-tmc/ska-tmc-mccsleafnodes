"""
Test cases in this module verify the functionality of the MCCS Subarray
component manager in different operational states.
"""

import pytest

from tests.settings import create_cm_mccssln


@pytest.mark.mccssln
def test_mccs_sln_working():
    """
    Test the functionality of the MCCS Subarray when it is in a working state.
    """
    cm = create_cm_mccssln()
    dev_info = cm.get_device()
    assert dev_info.unresponsive is False


@pytest.mark.mccssln
def test_mccs_sln_faulty():
    """
    Test the functionality of the MCCS Subarray when it is in a faulty state.
    """
    cm = create_cm_mccssln()
    dev_info = cm.get_device()
    dev_info.update_unresponsive(True)
    assert dev_info.unresponsive
