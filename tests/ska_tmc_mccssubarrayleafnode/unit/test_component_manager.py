"""Test cases file for component manager"""

import pytest
from ska_tango_base.control_model import ObsState
from ska_tmc_common.exceptions import DeviceUnresponsive

from tests.settings import MCCS_SUBARRAY_DEVICE, create_cm_mccssln


@pytest.mark.mccssln
def test_check_if_mccs_subarray_is_responsive():
    # Test when device is responsive
    cm = create_cm_mccssln()
    cm.get_device().update_unresponsive(False)
    assert cm._check_if_mccs_subarray_is_responsive() is None

    # Test when device is unresponsive
    with pytest.raises(DeviceUnresponsive):
        cm.get_device().update_unresponsive(True)
        cm._check_if_mccs_subarray_is_responsive()


@pytest.mark.mccssln
def test_update_device_obs_state():
    cm = create_cm_mccssln()
    cm.update_device_obs_state(MCCS_SUBARRAY_DEVICE, ObsState.CONFIGURING)
    assert cm.get_device().obs_state == ObsState.CONFIGURING
    assert cm.get_device().last_event_arrived is not None
    assert cm.get_device().unresponsive is False


@pytest.mark.mccssln
def test_mccssln_faulty():
    cm = create_cm_mccssln()
    dev_info = cm.get_device()
    dev_info.update_unresponsive(True)
    assert dev_info.unresponsive
