import pytest
from ska_tango_base.control_model import (
    AdminMode,
    ControlMode,
    SimulationMode,
    TestMode,
)
from ska_tmc_common import DevFactory
from ska_tmc_common.test_helpers.helper_subarray_device import (
    HelperSubArrayDevice,
)
from tango import DevState

from ska_tmc_mccssubarrayleafnode import LowTmcLeafNodeMccsSubarray, release
from tests.settings import MCCS_SUBARRAY_DEVICE, MCCS_SUBARRAY_LEAF_NODE_DEVICE


@pytest.fixture()
def devices_to_load():
    """Returns all devices to load"""
    return (
        {
            "class": LowTmcLeafNodeMccsSubarray,
            "devices": [
                {"name": MCCS_SUBARRAY_LEAF_NODE_DEVICE},
            ],
        },
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": MCCS_SUBARRAY_DEVICE},
            ],
        },
    )


@pytest.mark.mccssln
def test_attributes(tango_context):
    mccssln_device = DevFactory().get_device(MCCS_SUBARRAY_LEAF_NODE_DEVICE)
    assert mccssln_device.State() == DevState.ON
    mccssln_device.loggingTargets = ["console::cout"]
    assert "console::cout" in mccssln_device.loggingTargets
    mccssln_device.testMode = TestMode.NONE
    assert mccssln_device.testMode == TestMode.NONE
    mccssln_device.simulationMode = SimulationMode.FALSE
    assert mccssln_device.testMode == SimulationMode.FALSE
    mccssln_device.controlMode = ControlMode.REMOTE
    assert mccssln_device.controlMode == ControlMode.REMOTE
    mccssln_device.mccsSubarrayDevName = MCCS_SUBARRAY_DEVICE
    assert mccssln_device.mccsSubarrayDevName == MCCS_SUBARRAY_DEVICE
    assert mccssln_device.versionId == release.version
    assert mccssln_device.buildState == (
        "{},{},{}".format(release.name, release.version, release.description)
    )
    assert mccssln_device.mccsSubarrayAdminMode == AdminMode.ONLINE
    assert mccssln_device.isAdminModeEnabled is True


@pytest.mark.mccssln
def test_read_isSubsystemAvailable(tango_context):
    mccssln_device = DevFactory().get_device(MCCS_SUBARRAY_LEAF_NODE_DEVICE)
    assert not mccssln_device.isSubsystemAvailable
