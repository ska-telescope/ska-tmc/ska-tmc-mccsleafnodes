import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_tango_base.commands import ResultCode
from tango import Database, DeviceProxy

from tests.settings import RESULTCODE_OK


@given(
    "a TANGO ecosystem with a set of devices deployed",
    target_fixture="device_list",
)
def device_list():
    db = Database()
    return db.get_device_exported("*")


@given(
    parsers.parse("a LowTmcLeafNodeMccs device"),
    target_fixture="mccsmasterleaf_node",
)
def mccsmasterleaf_node(set_mccs_device_admin_mode_for_integration_tests):
    database = Database()
    instance_list = database.get_device_exported_for_class(
        "LowTmcLeafNodeMccs"
    )
    return DeviceProxy(instance_list.value_string[0])


@when(parsers.parse("I call the command {command_name}"))
def call_command(mccsmasterleaf_node, command_name, json_factory):
    try:
        input_str = ""
        match command_name:
            case "AssignResources":
                input_str = json_factory("command_AssignResources")
            case "ReleaseAllResources":
                input_str = json_factory("command_Release")

        if input_str:
            pytest.command_result = mccsmasterleaf_node.command_inout(
                command_name, input_str
            )
        else:
            pytest.command_result = mccsmasterleaf_node.command_inout(
                command_name
            )

    except Exception as ex:
        assert "DeviceUnresponsive" in str(ex)
        pytest.command_result = "DeviceUnresponsive"


@then(parsers.parse("the command is queued and executed successfully"))
def check_command(mccsmasterleaf_node, group_callback):
    if pytest.command_result == "CommandNotAllowed":
        return

    assert pytest.command_result[0][0] == ResultCode.QUEUED
    unique_id = pytest.command_result[1][0]
    mccsmasterleaf_node.subscribe_event(
        "longRunningCommandIDsInQueue",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandIDsInQueue"],
    )
    mccsmasterleaf_node.subscribe_event(
        "longRunningCommandsInQueue",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandsInQueue"],
    )
    mccsmasterleaf_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    group_callback["longRunningCommandIDsInQueue"].assert_change_event(
        (str(unique_id),), lookahead=8
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, RESULTCODE_OK), lookahead=12
    )

    group_callback["longRunningCommandsInQueue"].assert_change_event(
        (),
        lookahead=8,
    )


scenarios("../ska-tmc-mccsmasterleafnode/features/mccsmasterleafnode.feature")
