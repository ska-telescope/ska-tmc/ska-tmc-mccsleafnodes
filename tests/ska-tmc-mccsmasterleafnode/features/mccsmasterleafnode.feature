@XTP-28121
Feature: LowTmcLeafNodeMccs acceptance

	#Test the ability to generically run a a set of commands and that the execution is completed withing 5 seconds.
	@XTP-28122 @post_deployment @acceptance @SKA_low
	Scenario: Ability to run commands on LowTmcLeafNodeMccs
		Given a LowTmcLeafNodeMccs device
		When I call the command <command_name>
		Then the command is queued and executed successfully

		Examples:
		| command_name		  |
		| On                  |
		| AssignResources     |
		| ReleaseAllResources |
		| Standby             |
		| Off                 |


	#Check LowTmcLeafNodeMccs node correctly report failed and working devices defined within its scope of monitoring (internal model)
	@XTP-28124 @post_deployment @acceptance @SKA_low
	Scenario Outline: Monitor LowTmcLeafNodeMccs sub-devices
		Given a TANGO ecosystem with a set of devices deployed