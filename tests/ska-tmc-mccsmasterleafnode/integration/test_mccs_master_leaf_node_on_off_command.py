"""
This module contains test functions for the
'On' and 'Off' command in a Mccs master leaf node.
"""

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common import DevFactory
from tango import DevState

from tests.settings import (
    MCCS_CONTROLLER_DEVICE,
    MCCS_MASTER_LN_DEVICE,
    RESULTCODE_OK,
    logger,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_on_off_command(
    tango_context,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test the 'On' and Off' command in
    a Mccs master leaf node.

    This function performs the following steps:
    1. Gets the Mccs master leaf node.
    2. Subscribes to the
    'longRunningCommandsInQueue' event.
    3. Asserts the initial state of the event.
    4. Executes the 'Off' and 'On' command and checks the result.
    5. Subscribes to the 'longRunningCommandResult' event.
    6. Asserts the result of the 'On' and 'Off' command.
    7. Cleans up event subscriptions.

    This test checks the behavior of
    the 'On' and 'Off' command in a Mccs master leaf node.
    """
    dev_factory = DevFactory()
    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandsInQueue",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandsInQueue"],
    )
    group_callback["longRunningCommandsInQueue"].assert_change_event(())
    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    result, unique_id = mccsmln_node.On()
    group_callback["longRunningCommandsInQueue"].assert_change_event(
        ("On",), lookahead=8
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON
    result, unique_id = mccsmln_node.Off()
    group_callback["longRunningCommandsInQueue"].assert_change_event(
        ("On", "Off"), lookahead=8
    )
    logger.info("Command ID: %s Returned result: %s", unique_id, result)
    assert result[0] == ResultCode.QUEUED

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.OFF
    group_callback["longRunningCommandsInQueue"].assert_change_event(
        (), lookahead=8
    )
