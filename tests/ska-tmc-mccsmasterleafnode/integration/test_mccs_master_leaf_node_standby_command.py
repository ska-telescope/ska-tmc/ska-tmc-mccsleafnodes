"""
This module contains test functions for the
'On' and 'Standby' command in a Mccs master leaf node.
"""

import pytest
import tango
from ska_tmc_common import DevFactory
from tango import DevState

from tests.settings import (
    MCCS_CONTROLLER_DEVICE,
    MCCS_MASTER_LN_DEVICE,
    RESULTCODE_OK,
    tear_down_mccsmln,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_standby_command(
    tango_context,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test the 'Standby' command in
    a Mccs master leaf node.

    This function performs the following steps:
    1. Gets the Mccs master leaf node.
    2. Subscribes to the
    'longRunningCommandsInQueue' event.
    3. Asserts the initial state of the event.
    4. Executes the 'On' and 'Standby' command and checks the result.
    5. Subscribes to the 'longRunningCommandResult' event.
    6. Asserts the result of the 'On' and 'Standby' command.
    7. Calls tear_down method to teardown.
    8. Cleans up event subscriptions.

    This test checks the behavior of
    the 'On' and 'Standby' command in a Mccs master leaf node.
    """

    dev_factory = DevFactory()
    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandsInQueue",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandsInQueue"],
    )
    group_callback["longRunningCommandsInQueue"].assert_change_event(())
    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    _, unique_id = mccsmln_node.On()
    group_callback["longRunningCommandsInQueue"].assert_change_event(
        ("On",), lookahead=8
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON

    _, unique_id = mccsmln_node.Standby()
    group_callback["longRunningCommandsInQueue"].assert_change_event(
        ("On", "Standby"), lookahead=8
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.STANDBY

    tear_down_mccsmln(mccsmln_node, group_callback)
