import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory
from tango import DevState

from tests.settings import (
    COMMAND_NOT_ALLOWED_AFTER_QUEUING,
    ERROR_PROPAGATION_DEFECT,
    MCCS_CONTROLLER_DEVICE,
    MCCS_MASTER_LN_DEVICE,
    RESET_DEFECT,
    RESULTCODE_OK,
    STUCK_IN_INTERMEDIATE_STATE_DEFECT,
    TIMEOUT_OCCURRED,
    logger,
    tear_down_mccsmln,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_resource_command(
    tango_context,
    json_factory,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
) -> None:
    """
    Test assign resource functionality
    """
    dev_factory = DevFactory()

    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    result, unique_id = mccsmln_node.On()

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON

    assign_input_str = json_factory("command_AssignResources")
    result, unique_id = mccsmln_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK),
        lookahead=8,
    )

    logger.info(f"AssignResources Command ID: {unique_id}  completed")
    tear_down_mccsmln(mccsmln_node, group_callback)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_resource_command_timeout(
    tango_context,
    json_factory,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
) -> None:
    """
    Test assign resource timeout functionality
    """
    dev_factory = DevFactory()

    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    result, unique_id = mccsmln_node.On()

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON
    mccs_controller.SetDefective(STUCK_IN_INTERMEDIATE_STATE_DEFECT)

    assign_input_str = json_factory("command_AssignResources")
    result, unique_id = mccsmln_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    group_callback.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], TIMEOUT_OCCURRED),
        lookahead=20,
    )

    logger.info(f"AssignResources Command ID: {unique_id}  completed")

    mccs_controller.SetDefective(RESET_DEFECT)
    tear_down_mccsmln(mccsmln_node, group_callback)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_resource_command_error_propagation(
    tango_context,
    json_factory,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
) -> None:
    """
    Test assign resource error propagation functionality
    """
    dev_factory = DevFactory()

    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    result, unique_id = mccsmln_node.On()

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON
    mccs_controller.SetDefective(ERROR_PROPAGATION_DEFECT)

    assign_input_str = json_factory("command_AssignResources")
    result, unique_id = mccsmln_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    group_callback.assert_change_event(
        "longRunningCommandResult",
        (
            unique_id[0],
            f'[3, "Exception occurred on device: {MCCS_CONTROLLER_DEVICE}: '
            'Exception occured, command failed."]',
        ),
        lookahead=8,
    )

    logger.info(f"AssignResources Command ID: {unique_id}  completed")

    mccs_controller.SetDefective(RESET_DEFECT)
    tear_down_mccsmln(mccsmln_node, group_callback)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_resource_command_command_queuing(
    tango_context,
    json_factory,
    group_callback,
    set_mccs_device_admin_mode_for_integration_tests,
) -> None:
    """
    Test assign resource timeout functionality
    """
    dev_factory = DevFactory()

    mccs_controller = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccsmln_node = dev_factory.get_device(MCCS_MASTER_LN_DEVICE)

    mccsmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    result, unique_id = mccsmln_node.On()

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK), lookahead=8
    )
    assert mccs_controller.State() == DevState.ON
    mccs_controller.SetDefective(COMMAND_NOT_ALLOWED_AFTER_QUEUING)

    assign_input_str = json_factory("command_AssignResources")
    result, unique_id = mccsmln_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    group_callback.assert_change_event(
        "longRunningCommandResult",
        (
            unique_id[0],
            '[3, "Exception occurred on device:'
            f' {MCCS_CONTROLLER_DEVICE}: Command is not allowed"]',
        ),
        lookahead=20,
    )

    logger.info(f"AssignResources Command ID: {unique_id}  completed")

    mccs_controller.SetDefective(RESET_DEFECT)
    tear_down_mccsmln(mccsmln_node, group_callback)
