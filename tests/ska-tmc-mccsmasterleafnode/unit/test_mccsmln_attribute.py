"""
This module contains test functions
for checking the attributes of the
Mccs Master Leaf Node device.
"""

import pytest
from ska_tango_base.control_model import (
    AdminMode,
    ControlMode,
    SimulationMode,
    TestMode,
)
from ska_tmc_common import DevFactory
from ska_tmc_common.test_helpers.helper_mccs_controller_device import (
    HelperMCCSController,
)
from tango import DevState

from ska_tmc_mccsmasterleafnode import release
from ska_tmc_mccsmasterleafnode.mccs_master_leaf_node import LowTmcLeafNodeMccs
from tests.settings import MCCS_CONTROLLER_DEVICE, MCCS_MASTER_LN_DEVICE


@pytest.fixture()
def devices_to_load():
    """Returns all devices to load"""
    return (
        {
            "class": LowTmcLeafNodeMccs,
            "devices": [
                {"name": MCCS_MASTER_LN_DEVICE},
            ],
        },
        {
            "class": HelperMCCSController,
            "devices": [
                {"name": MCCS_CONTROLLER_DEVICE},
            ],
        },
    )


@pytest.mark.mccsmln
def test_attributes(tango_context):
    """
    Test the attributes of the
    Mccs Master Leaf Node device.

    This test checks various attributes
    of the Mccs Master Leaf Node device
    such as State, loggingTargets,
    testMode, simulationMode, controlMode,
    mccsMasterDevName, versionId, and buildState.

    It ensures that these attributes
    can be set and retrieved correctly.
    """
    mccsmln_device = DevFactory().get_device(MCCS_MASTER_LN_DEVICE)
    assert mccsmln_device.State() == DevState.ON
    mccsmln_device.loggingTargets = ["console::cout"]
    assert "console::cout" in mccsmln_device.loggingTargets
    mccsmln_device.testMode = TestMode.NONE
    assert mccsmln_device.testMode == TestMode.NONE
    mccsmln_device.simulationMode = SimulationMode.FALSE
    assert mccsmln_device.testMode == SimulationMode.FALSE
    mccsmln_device.controlMode = ControlMode.REMOTE
    assert mccsmln_device.controlMode == ControlMode.REMOTE
    mccsmln_device.mccsMasterDevName = MCCS_MASTER_LN_DEVICE
    assert mccsmln_device.mccsMasterDevName == MCCS_MASTER_LN_DEVICE
    assert mccsmln_device.versionId == release.version
    assert (
        mccsmln_device.buildState
        == f"{release.name},{release.version},{release.description}"
    )
    assert mccsmln_device.mccsControllerAdminMode == AdminMode.ONLINE
    assert mccsmln_device.isAdminModeEnabled is True
