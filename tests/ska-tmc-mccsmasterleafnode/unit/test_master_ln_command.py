import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import DevFactory

from ska_tmc_mccsmasterleafnode.commands.assign_resources_command import (
    AssignResources,
)
from tests.settings import MCCS_CONTROLLER_DEVICE, get_mccsmln_command_obj


@pytest.mark.mccsmln
def test_mccs_mln_command(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
) -> None:
    DevFactory().get_device(MCCS_CONTROLLER_DEVICE).On()
    cm, command_obj = get_mccsmln_command_obj(
        AssignResources, MCCS_CONTROLLER_DEVICE
    )

    task_status, message = command_obj.reject_command("Command is Rejected")
    assert task_status == TaskStatus.REJECTED
    assert message == "Command is Rejected"
    result_code, message = command_obj.init_adapter()
    assert result_code == ResultCode.OK
    assert message == "Adapter initialisation is successful"


@pytest.mark.mccsmln
def test_adapter_creation_success(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
) -> None:
    _, command_obj = get_mccsmln_command_obj(
        AssignResources, MCCS_CONTROLLER_DEVICE
    )

    result_code, message = command_obj.init_adapter()
    assert result_code == ResultCode.OK
    assert message == "Adapter initialisation is successful"


@pytest.mark.mccsmln
def test_adapter_creation_timeout(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
) -> None:
    cm, command_obj = get_mccsmln_command_obj(
        AssignResources, MCCS_CONTROLLER_DEVICE
    )
    cm.mccs_master_device_name = ""
    result_code, message = command_obj.init_adapter()
    assert result_code == ResultCode.FAILED
    assert "Error in creating adapter for" in message
