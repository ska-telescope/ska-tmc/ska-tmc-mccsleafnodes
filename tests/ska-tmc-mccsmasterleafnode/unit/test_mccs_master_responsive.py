"""
This module contains test cases to check the faultiness of devices.
"""

import pytest

from tests.settings import MCCS_MASTER_LN_DEVICE, create_cm_mccsmln


@pytest.mark.mccsmln
def test_mccs_master_working():
    """
    The test is checking whether the device is responsive.
    """
    cm = create_cm_mccsmln(MCCS_MASTER_LN_DEVICE)
    dev_info = cm.get_device()
    assert dev_info.unresponsive is False


@pytest.mark.mccsmln
def test_mccs_master_faulty():
    """
    Test the functionality of the Mccs Master when it is in a faulty state.
    """
    cm = create_cm_mccsmln(MCCS_MASTER_LN_DEVICE)
    dev_info = cm.get_device()
    dev_info.update_unresponsive(True)
    assert dev_info.unresponsive
