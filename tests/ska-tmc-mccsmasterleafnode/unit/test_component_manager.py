"""Test cases file for component manager"""

import pytest
from ska_tmc_common import DeviceUnresponsive

from tests.settings import MCCS_CONTROLLER_DEVICE, create_cm_mccsmln


@pytest.mark.mccsmln
def test_check_if_mccs_master_is_responsive():
    # Test when device is responsive
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    cm.get_device().update_unresponsive(False)
    assert cm.is_mccs_master_responsive() is None

    # Test when device is unresponsive
    with pytest.raises(DeviceUnresponsive):
        cm.get_device().update_unresponsive(True)
        cm.is_mccs_master_responsive()


@pytest.mark.mccsmln
def test_validate_json_argument_low():
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)

    # Valid JSON
    valid_json = {"key1": "value1", "key2": "value2"}
    valid_keys = ["key1", "key2"]
    is_valid, message = cm.validate_json_argument_low(valid_json, valid_keys)
    assert is_valid
    assert message == "Received json has required fields"

    # Invalid JSON (missing key2)
    invalid_json = {"key1": "value1"}
    is_valid, message = cm.validate_json_argument_low(invalid_json, valid_keys)
    assert not is_valid
    assert message == "key2 key is not present in the input json argument."


@pytest.mark.mccsmln
def test_clear_command_mapping():
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)

    # Add a command mapping
    cm.command_mapping["unique_id"] = "command_id"

    # Clear the command mapping
    cm.clear_command_mapping("unique_id")

    # Assert command mapping is cleared
    assert "unique_id" not in cm.command_mapping
