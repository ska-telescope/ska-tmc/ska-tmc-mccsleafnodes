import threading

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    DeviceUnresponsive,
    HelperAdapterFactory,
)
from ska_tmc_common.dev_factory import DevFactory

from ska_tmc_mccsmasterleafnode.commands.release_all_resources_command import (
    ReleaseAllResources,
)
from tests.settings import (
    MCCS_CONTROLLER_DEVICE,
    create_cm_mccsmln,
    get_mccsmln_command_obj,
    logger,
)


@pytest.mark.mccsmln
def test_release_command_completed(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
) -> None:
    cm, command = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    DevFactory().get_device(MCCS_CONTROLLER_DEVICE).On()
    assert cm.is_command_allowed("ReleaseAllResources")
    release_input_str = json_factory("command_Release")
    cm.release_resources(release_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.mccsmln
def test_release_command_completed_fail(
    tango_context, json_factory, set_mccs_device_admin_mode_for_unit_tests
) -> None:
    cm, _ = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    assert cm.is_command_allowed("ReleaseAllResources")

    adapter_factory = HelperAdapterFactory()
    attrs = {"ReleaseAllResources.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        MCCS_CONTROLLER_DEVICE, AdapterType.MCCS_CONTROLLER, proxy=subarrayMock
    )

    release_resources = ReleaseAllResources(cm, logger)
    release_resources.adapter_factory = adapter_factory

    release_input_str = json_factory("command_Release")
    (result_code, message) = release_resources.do(release_input_str)
    assert result_code == ResultCode.FAILED
    assert MCCS_CONTROLLER_DEVICE in message


@pytest.mark.mccsmln
def test_release_command_fail_check_allowed_with_device_unresponsive(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    cm, _ = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    with pytest.raises(DeviceUnresponsive):
        cm._device.update_unresponsive(True)
        cm.is_command_allowed("ReleaseAllResources")


@pytest.mark.mccsmln
def test_release_resources_command_fail_check_allowed_with_invalid_obsState(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
) -> None:
    cm, _ = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    assert not cm.is_command_allowed_callable("ReleaseAllResources")()


@pytest.mark.mccsmln
def test_release_resources_command_empty_input_json(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
) -> None:
    cm, command = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    assert cm.is_command_allowed("ReleaseAllResources")
    (result_code, _) = command.do("")
    logger.info(f"result_code - {result_code}")
    assert result_code == ResultCode.FAILED


@pytest.mark.mccsmln
def test_release_resources_command_timeout(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    attrs = {
        "Release.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(f"Timeout occurred for {MCCS_CONTROLLER_DEVICE}")
        )
    }
    mccscontrollerMock = mock.Mock(**attrs)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_CONTROLLER_DEVICE,
        AdapterType.MCCS_CONTROLLER,
        proxy=mccscontrollerMock,
    )
    release_command = ReleaseAllResources(cm, logger)
    release_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("ReleaseAllResources")

    release_input_str = json_factory("command_Release")
    release_command.release_all_resources(
        argin=release_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Release command is failed "
            f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
            "The following exception occurred - "
            f"Timeout occurred for {MCCS_CONTROLLER_DEVICE}.",
        ),
        exception="The invocation of the Release command is failed "
        f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
        "The following exception occurred - "
        f"Timeout occurred for {MCCS_CONTROLLER_DEVICE}.",
    )


@pytest.mark.mccsmln
def test_release_resources_command_error_propagation(
    tango_context,
    task_callback,
    json_factory,
    set_mccs_device_admin_mode_for_unit_tests,
):
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    attrs = {
        "Release.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(f"Exception occurred for {MCCS_CONTROLLER_DEVICE}")
        )
    }
    mccscontrollerMock = mock.Mock(**attrs)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_CONTROLLER_DEVICE,
        AdapterType.MCCS_CONTROLLER,
        proxy=mccscontrollerMock,
    )
    release_command = ReleaseAllResources(cm, logger)
    release_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("ReleaseAllResources")

    release_input_str = json_factory("command_Release")
    release_command.release_all_resources(
        argin=release_input_str,
        task_callback=task_callback,
        task_abort_event=threading.Event,
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Release command is failed "
            f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
            "The following exception occurred - "
            f"Exception occurred for {MCCS_CONTROLLER_DEVICE}.",
        ),
        exception="The invocation of the Release command is failed "
        f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
        "The following exception occurred - "
        f"Exception occurred for {MCCS_CONTROLLER_DEVICE}.",
    )


@pytest.mark.mccsmln
def test_telescope_release_resources_command_ResultCode(
    tango_context, json_factory, set_mccs_device_admin_mode_for_unit_tests
) -> None:
    cm, command = get_mccsmln_command_obj(
        ReleaseAllResources, MCCS_CONTROLLER_DEVICE
    )
    assert cm.is_command_allowed("ReleaseAllResources")
    release_json = json_factory("command_Release")
    logger.info(release_json)
    (result_code, _) = command.do(release_json)
    logger.info(f"result_code - {result_code}")
    assert result_code == ResultCode.STARTED
