import pytest
from ska_control_model import AdminMode
from ska_tango_base.commands import ResultCode

from ska_tmc_mccsmasterleafnode.commands.set_controller_admin_mode import (
    SetAdminMode,
)
from tests.settings import MCCS_CONTROLLER_DEVICE, create_cm_mccsmln, logger


@pytest.mark.mccsmln
def test_set_admin_mode_command_low(tango_context):
    """
    Test the successful
    execution of the SetAdminMode command.
    """
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    argin = AdminMode.ONLINE
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)
    assert result_code == ResultCode.OK
    assert message == "Command Completed"


@pytest.mark.mccsmln
def test_invalid_admin_mode_command_low(
    tango_context,
):
    """Test to set the adminMode on mccs controller"""
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)

    argin = 7  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.FAILED
    assert message == "Command Failed"


@pytest.mark.mccsmln
def test_feature_toggle_adminMode(tango_context):
    """Test to set the adminMode on mccs master"""
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)

    cm.is_admin_mode_enabled = False
    argin = AdminMode.ONLINE  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.NOT_ALLOWED
    assert message == (
        "AdminMode functionality is disabled, "
        + "Device will function normally."
    )
