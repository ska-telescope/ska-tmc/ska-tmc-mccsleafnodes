"""Test cases to verify
 Mccs Master Leaf Node On command"""

import mock
import pytest
from ska_tango_base.commands import ResultCode, TaskStatus
from ska_tmc_common import (
    AdapterType,
    CommandNotAllowed,
    DeviceUnresponsive,
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_mccsmasterleafnode.commands import On
from tests.settings import MCCS_CONTROLLER_DEVICE, create_cm_mccsmln, logger


@pytest.mark.mccsmln
def test_on_command(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test the successful
    execution of the On command.

    This test case checks whether
    the On command can be executed successfully.
    It verifies that the command is allowed,
    queues the command, marks it as in progress,
    and finally marks it as
    completed with a successful result code.
    """
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    assert cm.is_command_allowed("On")
    cm.on_command(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.mccsmln
def test_on_command_fail_mccs_master(
    tango_context, task_callback, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test the On command failure when
    the Mccs master device encounters an exception.

    This test case checks whether
     the On command can handle the failure scenario
    when the Mccs master device raises
    an exception during execution. It verifies that
    the command is allowed, executes
    the On command, marks it as in progress, and
    finally marks it as completed with a failed result code.
    """
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    attrs = {
        "On.side_effect": lambda *args, **kwargs: (_ for _ in ()).throw(
            Exception(f" {MCCS_CONTROLLER_DEVICE} device unavailable")
        )
    }
    mccscontrollerMock = mock.Mock(**attrs)
    adapter_factory = HelperAdapterFactory()
    adapter_factory.get_or_create_adapter(
        MCCS_CONTROLLER_DEVICE, AdapterType.BASE, proxy=mccscontrollerMock
    )
    on_command = On(cm, logger)
    on_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("On")
    on_command.on(logger, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the On command is failed "
            + f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
            + "The following exception occurred -"
            + f"  {MCCS_CONTROLLER_DEVICE} device unavailable.",
        ),
        exception="The invocation of the On command is failed "
        + f"on MCCS Controller device {MCCS_CONTROLLER_DEVICE}.\n"
        + "The following exception occurred -"
        + f"  {MCCS_CONTROLLER_DEVICE} device unavailable.",
    )


@pytest.mark.mccsmln
def test_on_command_is_not_allowed_device_unresponsive(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test the On command when
    the Mccs master device is unresponsive.

    This test case checks whether
    the On command correctly raises a DeviceUnresponsive
    exception when the Mccs
    master device is unresponsive.
    """
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    with pytest.raises(DeviceUnresponsive):
        cm._device.update_unresponsive(True)
        cm.is_command_allowed("Off")


@pytest.mark.mccsmln
def test_on_fail_is_allowed(
    tango_context, set_mccs_device_admin_mode_for_unit_tests
):
    """
    Test the On command when the
    Mccs master leaf node is in a faulty state.

    This test case checks whether
    the On command correctly raises a CommandNotAllowed
    exception when the Mccs master leaf node
    is in a faulty state and the command is
    not allowed.
    """
    cm = create_cm_mccsmln(MCCS_CONTROLLER_DEVICE)
    cm.op_state_model._op_state = DevState.FAULT
    with pytest.raises(CommandNotAllowed):
        cm.is_command_allowed("On")
