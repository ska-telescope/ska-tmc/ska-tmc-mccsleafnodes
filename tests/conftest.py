# pylint: disable=redefined-outer-name
"""
Conftest file for MCCS Leaf Node
"""

import logging
from os.path import dirname, join
from typing import Callable

import pytest
import tango
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from ska_tmc_common import (
    DevFactory,
    HelperMCCSController,
    HelperMccsSubarrayDevice,
)
from tango import DeviceProxy
from tango.test_context import MultiDeviceTestContext
from tango.test_utils import DeviceTestContext

from ska_tmc_mccsmasterleafnode.mccs_master_leaf_node import LowTmcLeafNodeMccs
from ska_tmc_mccssubarrayleafnode.mccs_subarray_leaf_node import (
    LowTmcLeafNodeMccsSubarray,
)
from tests.settings import (
    MCCS_CONTROLLER_DEVICE,
    MCCS_MASTER_LN_DEVICE,
    MCCS_SUBARRAY_DEVICE,
)


def pytest_addoption(parser) -> None:
    """
    Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no
    need for a :py:class:`tango.test_context.MultiDeviceTestContext`.
    :param parser: the command line options parser
    :type parser: :py:class:`argparse.ArgumentParser`
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


# pylint: disable=unused-argument
def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: class:`pytest.Session`
    """
    print(tango.utils.info())


@pytest.fixture()
def devices_to_load():
    """Returns all devices to load"""
    return (
        {
            "class": HelperMCCSController,
            "devices": [
                {"name": MCCS_CONTROLLER_DEVICE},
            ],
        },
        {
            "class": HelperMccsSubarrayDevice,
            "devices": [
                {"name": MCCS_SUBARRAY_DEVICE},
            ],
        },
    )


@pytest.fixture
def group_callback() -> MockTangoEventCallbackGroup:
    """Creates a mock callback group for asynchronous testing

    :rtype: MockTangoEventCallbackGroup
    """
    group_callback = MockTangoEventCallbackGroup(
        "longRunningCommandsInQueue",
        "longRunningCommandResult",
        "longRunningCommandIDsInQueue",
        "obsState",
        timeout=50,
    )
    return group_callback


@pytest.fixture
def mccsmln_device(request):
    """Create DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowTmcLeafNodeMccs) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "LowTmcLeafNodeMccs"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


# pylint: disable= protected-access
@pytest.fixture
def tango_context(devices_to_load, request):
    """Provide context to run devices without database"""
    true_context = request.config.getoption("--true-context")
    logging.info("true context: %s", true_context)
    if not true_context:
        with MultiDeviceTestContext(
            devices_to_load, process=True, timeout=30
        ) as context:
            DevFactory._test_context = context
            logging.info("test context set right")
            yield context
    else:
        yield None


@pytest.fixture
def task_callback() -> MockCallable:
    """Creates a mock callable for asynchronous testing

    :rtype: MockCallable
    """

    task_callback = MockCallable(30)

    return task_callback


@pytest.fixture(scope="module")
def mccssln_device(request):
    """Create DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowTmcLeafNodeMccsSubarray) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "LowTmcLeafNodeMccsSubarray"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


def get_input_str(file_name: str) -> str:
    """
    Returns input json string.

    :param file_name: The file name for input json
    :file_name dtype: str

    :rtype: String
    """
    with open(file_name, "r", encoding="utf-8") as f:
        input_str = f.read()
    return input_str


@pytest.fixture()
def json_factory() -> Callable:
    """
    Json factory for getting json files.
    """

    def _get_json(file_name) -> str:
        """Returns the input json in string format.

        :param file_name: The file name for input json
        :file_name dtype: str

        :rtype: str
        """
        return get_input_str(
            join(dirname(__file__), "data", f"{file_name}.json")
        )

    return _get_json


@pytest.fixture(scope="module")
def mccs_subarray():
    """Returns the DeviceProxy for MCCS Subarray"""
    return DeviceProxy(MCCS_SUBARRAY_DEVICE)


@pytest.fixture(scope="module")
def mccs_master_leaf_node():
    """Returns the DeviceProxy for MCCS Master Leaf Node"""
    return DeviceProxy(MCCS_MASTER_LN_DEVICE)


@pytest.fixture(scope="module")
def mccs_controller():
    """Returns the DeviceProxy for MCCS Master Leaf Node"""
    return DeviceProxy(MCCS_CONTROLLER_DEVICE)


@pytest.fixture()
def set_mccs_device_admin_mode_for_integration_tests():
    """Set the admin mode of MCCS devices"""
    mccs_master_device = tango.DeviceProxy(MCCS_CONTROLLER_DEVICE)
    mccs_subarray_device = tango.DeviceProxy(MCCS_SUBARRAY_DEVICE)
    if mccs_master_device.adminMode != 0:
        mccs_master_device.adminMode = 0
    if mccs_subarray_device.adminMode != 0:
        mccs_subarray_device.adminMode = 0


@pytest.fixture()
def set_mccs_device_admin_mode_for_unit_tests():
    """Set the admin mode of MCCS devices"""
    dev_factory = DevFactory()
    mccs_master_device = dev_factory.get_device(MCCS_CONTROLLER_DEVICE)
    mccs_subarray_device = dev_factory.get_device(MCCS_SUBARRAY_DEVICE)
    if mccs_master_device.adminMode != 0:
        mccs_master_device.adminMode = 0
    if mccs_subarray_device.adminMode != 0:
        mccs_subarray_device.adminMode = 0
