import pytest
import tango
from pytest_bdd import given, parsers, then
from ska_control_model import ObsState
from ska_tango_base.commands import ResultCode

from tests.settings import (
    RESET_DEFECT,
    RESULTCODE_OK,
    logger,
    tear_down_mccsmln,
)


@given("an MCCS Subarray in the IDLE obsState")
def mccs_subarray_in_idle_state(
    mccsmln_device, mccssln_device, group_callback, json_factory
):
    """Sets up the MCCS Subarray device to be in IDLE ObsState"""
    try:
        LRCR_ID = mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        # Invoke On command on MCCS Master Leaf Node
        result, unique_id = mccsmln_device.On()
        assert result[0] == ResultCode.QUEUED

        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )

        OBS_STATE_ID = mccssln_device.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        assign_input_json = json_factory("command_AssignResources")
        result_code, unique_id = mccsmln_device.AssignResources(
            assign_input_json
        )
        assert result_code[0] == ResultCode.QUEUED
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )
        group_callback["obsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=12,
        )
        mccsmln_device.unsubscribe_event(LRCR_ID)
        mccssln_device.unsubscribe_event(OBS_STATE_ID)
    except Exception as exception:
        logger.exception(
            "Exception occured while setting up the MCCS Subarray: %s",
            exception,
        )
        pytest.fail(reason=str(exception))


@given(
    parsers.parse("an MCCS Subarray in the READY obsState"),
)
def mccs_subarray_in_ready_state(
    mccssln_device,
    mccs_subarray,
    mccsmln_device,
    json_factory,
    group_callback,
):
    """Sets up the MCCS Subarray device to be in READY ObsState."""
    try:
        LRCR_ID_MLN = mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        # Invoke On command on MCCS Master Leaf Node
        result, unique_id = mccsmln_device.On()
        assert result[0] == ResultCode.QUEUED

        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )

        OBS_STATE_ID = mccs_subarray.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        assign_input_json = json_factory("command_AssignResources")
        result_code, unique_id = mccsmln_device.AssignResources(
            assign_input_json
        )
        assert result_code[0] == ResultCode.QUEUED

        group_callback["obsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=12,
        )
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )
        mccs_subarray.unsubscribe_event(OBS_STATE_ID)
        OBS_STATE_SLN_ID = mccssln_device.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        group_callback["obsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=12,
        )

        configure_input_string = json_factory("mccssln_configure_json")
        result_code, unique_id = mccssln_device.Configure(
            configure_input_string
        )

        LRCR_ID_SLN = mccssln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        assert result_code[0] == ResultCode.QUEUED
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )
        group_callback["obsState"].assert_change_event(
            ObsState.READY,
            lookahead=12,
        )
        mccssln_device.unsubscribe_event(OBS_STATE_SLN_ID)
        mccsmln_device.unsubscribe_event(LRCR_ID_MLN)
        mccssln_device.unsubscribe_event(LRCR_ID_SLN)
    except Exception as exception:
        logger.exception(
            "Exception occured while setting up the MCCS Subarray: %s",
            exception,
        )
        pytest.fail(reason=str(exception))


@given(
    parsers.parse("an MCCS subarray in the SCANNING obsState"),
)
def mccs_subarray_in_scanning_state(
    mccssln_device,
    mccs_subarray,
    mccsmln_device,
    json_factory,
    group_callback,
):
    """Sets up the MCCS Subarray device to be in SCANNING ObsState."""
    try:
        LRCR_ID_MLN = mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        # Invoke On command on MCCS Master Leaf Node
        result, unique_id = mccsmln_device.On()
        assert result[0] == ResultCode.QUEUED

        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )

        OBS_STATE_ID = mccs_subarray.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        assign_input_json = json_factory("command_AssignResources")
        result_code, unique_id = mccsmln_device.AssignResources(
            assign_input_json
        )
        assert result_code[0] == ResultCode.QUEUED

        group_callback["obsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=12,
        )
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=12,
        )
        mccs_subarray.unsubscribe_event(OBS_STATE_ID)

        OBS_STATE_SLN_ID = mccssln_device.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        group_callback["obsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=12,
        )

        LRCR_ID_SLN = mccssln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        configure_input_string = json_factory("mccssln_configure_json")
        result_code, unique_id = mccssln_device.Configure(
            configure_input_string
        )

        assert result_code[0] == ResultCode.QUEUED

        group_callback["obsState"].assert_change_event(
            ObsState.READY,
            lookahead=12,
        )
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=12,
        )
        scan_input_str = json_factory("mccssln_scan_json")
        result_code, unique_id = mccssln_device.Scan(scan_input_str)

        assert result_code[0] == ResultCode.QUEUED

        group_callback["obsState"].assert_change_event(
            ObsState.SCANNING,
            lookahead=8,
        )
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=12,
        )
        mccssln_device.unsubscribe_event(OBS_STATE_SLN_ID)
        mccsmln_device.unsubscribe_event(LRCR_ID_MLN)
        mccssln_device.unsubscribe_event(LRCR_ID_SLN)
    except Exception as exception:
        logger.exception(
            "Exception occured while setting up the MCCS Subarray: %s",
            exception,
        )
        pytest.fail(reason=str(exception))


@then(parsers.parse("the command times out, reporting the timeout error."))
def verify_timeout_message(
    mccsmln_device,
    mccssln_device,
    mccs_subarray,
    group_callback,
):
    """Verify that the End command failed with the given error message"""
    unique_id = pytest.command_result[1][0]
    LRCR_ID_SLN = mccssln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, '[3, "Timeout has occurred, command failed"]'),
        lookahead=20,
    )
    mccs_subarray.SetDefective(RESET_DEFECT)
    assert mccs_subarray.defective == RESET_DEFECT
    mccssln_device.unsubscribe_event(LRCR_ID_SLN)
    tear_down_mccsmln(mccsmln_device, group_callback)


@then(
    parsers.parse(
        "Exception Occurs on MCCS Subarry leaf node and gets propagated"
    )
)
def verify_error_propagation_message(
    mccsmln_device,
    mccssln_device,
    mccs_subarray,
    group_callback,
):
    """Verify that the End command failed with the given error message"""
    message = '[3, "Exception occured, command failed."]'
    unique_id = pytest.command_result[1][0]
    LRCR_ID_SLN = mccssln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, message),
        lookahead=20,
    )
    mccs_subarray.SetDefective(RESET_DEFECT)
    assert mccs_subarray.defective == RESET_DEFECT
    mccssln_device.unsubscribe_event(LRCR_ID_SLN)
    tear_down_mccsmln(mccsmln_device, group_callback)
