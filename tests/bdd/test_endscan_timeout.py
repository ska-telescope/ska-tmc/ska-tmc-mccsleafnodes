"""
BDD Test cases of EndScan command verify timeout functionality.
"""

import pytest
from pytest_bdd import parsers, scenario, when
from ska_tango_base.commands import ResultCode

from tests.settings import STUCK_IN_INTERMEDIATE_STATE_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28262.feature",
    "MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for EndScan "
    + "command",
)
def test_endscan_timeout(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the Timeout functionality for EndScan command on
    MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke EndScan command on a defective MCCS Subarray"))
def endscan_mccs_sln(mccssln_device, mccs_subarray):
    """Invokes EndScan command on MCCS Subarray Leaf Node."""
    try:
        mccs_subarray.SetDefective(STUCK_IN_INTERMEDIATE_STATE_DEFECT)
        pytest.command_result = mccssln_device.EndScan()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while setting up the MCCS Subarray: %s",
            exception,
        )
        pytest.fail(
            reason="EndScan command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
