"""
BDD Test cases for Configure Command for Error Propagation Functionality.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""

import pytest
from pytest_bdd import parsers, scenario, when
from ska_tango_base.commands import ResultCode

from tests.settings import ERROR_PROPAGATION_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp-72319.feature",
    "MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for "
    + "Configure command",
)
def test_configure_error_propagation(
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test case to verify the Error propagation functionality for
    Configure command on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke Configure command on a defective MCCS Subarray"))
def configure_mccs_sln(mccssln_device, mccs_subarray, json_factory):
    """Invokes Configure command on MCCS Subarray Leaf Node."""
    try:
        mccs_subarray.SetDefective(ERROR_PROPAGATION_DEFECT)
        configure_input_json = json_factory("mccssln_configure_json")
        pytest.command_result = mccssln_device.Configure(configure_input_json)
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Configure on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Configure command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
