"""
BDD Test cases for EndScan Command.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""
import pytest
import tango
from pytest_bdd import parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import RESULTCODE_OK, logger, tear_down_mccsmln


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28256.feature",
    "MCCS Subarray Leaf Node executes EndScan successfully",
)
def test_endscan_command(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the EndScan functionality on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke EndScan command on MCCS Subarray Leaf Node"))
def endscan_mccs_sln(mccssln_device):
    """Invokes EndScan command on the MCCS Subarray Leaf Node."""
    try:
        pytest.command_result = mccssln_device.EndScan()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking EndScan on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="EndScan command failed on the MCCS Subarray Leaf Node."
        )


@then(
    parsers.parse("the ObsState changes from SCANNING to READY"),
)
def verify_endscan_completion(mccsmln_device, mccssln_device, group_callback):
    """Verifies that the MCCS Subarray ObsState successfully transitioned to
    READY ObsState."""

    unique_id = pytest.command_result[1][0]

    mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    mccssln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )

    group_callback["obsState"].assert_change_event(ObsState.READY, lookahead=8)
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, RESULTCODE_OK), lookahead=12
    )
    tear_down_mccsmln(mccsmln_device, group_callback)
