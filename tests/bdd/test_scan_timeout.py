"""
BDD Test cases of Scan command verify timeout functionality.
"""
import pytest
from pytest_bdd import parsers, scenario, when
from ska_tango_base.commands import ResultCode

from tests.settings import STUCK_IN_INTERMEDIATE_STATE_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28261.feature",
    "MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for Scan "
    + "command",
)
def test_scan_timeout(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the Timeout functionality for Scan command on
    MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke Scan command on a defective MCCS Subarray"))
def scan_mccs_sln(mccssln_device, mccs_subarray, json_factory):
    """Invokes Scan command on MCCS Subarray Leaf Node."""
    try:
        mccs_subarray.SetDefective(STUCK_IN_INTERMEDIATE_STATE_DEFECT)
        scan_input_json = json_factory("mccssln_scan_json")
        pytest.command_result = mccssln_device.Scan(scan_input_json)
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Scan on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Scan command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
