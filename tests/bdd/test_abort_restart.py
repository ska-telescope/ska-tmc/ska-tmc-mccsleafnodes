import json

import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import (
    RESET_DEFECT,
    RESULTCODE_OK,
    STUCK_IN_INTERMEDIATE_STATE_DEFECT,
    logger,
    tear_down_mccsmln,
)


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28309.feature",
    "MCCS Subarray Leaf Node executes Abort Restart successfully",
)
def test_abort_command(
    mccsmln_device, set_mccs_device_admin_mode_for_integration_tests
):
    """
    Test case to verify the Abort and Restart functionality on
    Mccs Subarray Leaf Node.
    """


@given(parsers.parse("an MCCS Subarray in the {obsState} obsState"))
def mccs_subarray_in_given_obs_state(
    mccsmln_device,
    mccs_subarray,
    mccssln_device,
    group_callback,
    json_factory,
    obsState,
):
    """Given an MCCS Subarray Leaf Node in particular obsState"""
    try:
        MCCSMLN_LRCR_ID = mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        # Invoke On command on MCCS Master Leaf Node
        result, unique_id = mccsmln_device.On()
        assert result[0] == ResultCode.QUEUED

        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )
        if obsState == "RESOURCING":
            mccs_subarray.subscribe_event(
                "obsState",
                tango.EventType.CHANGE_EVENT,
                group_callback["obsState"],
            )
            intermediate_state = ObsState.RESOURCING
            defect = json.loads(STUCK_IN_INTERMEDIATE_STATE_DEFECT)
            defect["intermediate_state"] = intermediate_state
            mccs_subarray.SetDefective(json.dumps(defect))
            assign_input_json = json_factory("command_AssignResources")
            result_code, unique_id = mccsmln_device.AssignResources(
                assign_input_json
            )
            assert result_code[0] == ResultCode.QUEUED

            group_callback["obsState"].assert_change_event(
                ObsState.RESOURCING,
                lookahead=8,
            )
            mccsmln_device.unsubscribe_event(MCCSMLN_LRCR_ID)
        elif obsState == "CONFIGURING":
            mccssln_device.subscribe_event(
                "obsState",
                tango.EventType.CHANGE_EVENT,
                group_callback["obsState"],
            )
            assign_input_json = json_factory("command_AssignResources")
            result_code, unique_id = mccsmln_device.AssignResources(
                assign_input_json
            )
            assert result_code[0] == ResultCode.QUEUED
            group_callback["obsState"].assert_change_event(
                ObsState.IDLE,
                lookahead=8,
            )
            group_callback["longRunningCommandResult"].assert_change_event(
                (unique_id[0], RESULTCODE_OK),
                lookahead=8,
            )

            intermediate_state = ObsState.CONFIGURING
            defect = json.loads(STUCK_IN_INTERMEDIATE_STATE_DEFECT)
            defect["intermediate_state"] = intermediate_state
            mccs_subarray.SetDefective(json.dumps(defect))
            configure_input_string = json_factory("mccssln_configure_json")
            result_code, unique_id = mccssln_device.Configure(
                configure_input_string
            )

            assert result_code[0] == ResultCode.QUEUED
            group_callback["obsState"].assert_change_event(
                ObsState.CONFIGURING,
                lookahead=8,
            )
            mccsmln_device.unsubscribe_event(MCCSMLN_LRCR_ID)

        mccs_subarray.SetDefective(RESET_DEFECT)

    except Exception as exception:
        logger.exception(
            "Exception occured while setting up the MCCS Subarray: %s",
            exception,
        )
        pytest.fail(reason=str(exception))


@when(parsers.parse("I invoke Abort command on the MCCS Subarray Leaf Node"))
def abort_mccs_sln(mccssln_device):
    """Invokes Abort command on MCCS Subarray Leaf Node."""
    try:
        pytest.command_result = mccssln_device.Abort()
        assert pytest.command_result[0][0] == ResultCode.STARTED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Abort on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Abort command failed on the MCCS Subarray Leaf Node."
        )


@then(parsers.parse("the MCCS Subarray obsState changes to ABORTED"))
def abort_completion(mccs_subarray, group_callback):
    """Verify the obsState is changed to ABORTED"""

    MCCS_SUBARRAY_OBS_STATE_ID = mccs_subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["obsState"].assert_change_event(
        ObsState.ABORTING,
        lookahead=8,
    )
    group_callback["obsState"].assert_change_event(
        ObsState.ABORTED,
        lookahead=8,
    )
    mccs_subarray.unsubscribe_event(MCCS_SUBARRAY_OBS_STATE_ID)


@then(parsers.parse("I invoke Restart command on the MCCS Subarray Leaf Node"))
def restart_mccs_sln(mccssln_device, group_callback):
    """Invokes Restart command on MCCS Subarray Leaf Node."""
    MCCS_SUBARRAY_LN_OBS_STATE_ID = mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["obsState"].assert_change_event(
        ObsState.ABORTED,
        lookahead=8,
    )
    mccssln_device.unsubscribe_event(MCCS_SUBARRAY_LN_OBS_STATE_ID)

    try:
        pytest.command_result = mccssln_device.Restart()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Restart on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Restart command failed on the MCCS Subarray Leaf Node."
        )


@then(parsers.parse("the Mccs Subarray changes obsState to the EMPTY"))
def restart_completion(mccs_subarray, group_callback, mccsmln_device):
    """Verify the obsState is changed to EMPTY"""
    MCCS_SUBARRAY_OBS_STATE_ID = mccs_subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["obsState"].assert_change_event(
        ObsState.RESTARTING,
        lookahead=8,
    )
    group_callback["obsState"].assert_change_event(
        ObsState.EMPTY,
        lookahead=8,
    )
    mccs_subarray.unsubscribe_event(MCCS_SUBARRAY_OBS_STATE_ID)
    tear_down_mccsmln(mccsmln_device, group_callback)
