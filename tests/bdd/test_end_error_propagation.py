"""
BDD Test cases for End Command for Error Propagation Functionality.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""

import pytest
from pytest_bdd import parsers, scenario, when
from ska_tango_base.commands import ResultCode

from tests.settings import ERROR_PROPAGATION_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp-72320.feature",
    "MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for End "
    + "command",
)
def test_end_error_propagation(
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test case to verify the Error propagation functionality for
    End command on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke END command on a defective MCCS Subarray"))
def end_command_mccs_sln(mccssln_device, mccs_subarray, json_factory):
    """Invokes End command on MCCS Subarray Leaf Node."""
    try:
        mccs_subarray.SetDefective(ERROR_PROPAGATION_DEFECT)
        pytest.command_result = mccssln_device.End()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking End on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="End command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
