"""
BDD Test cases for EndScan Command for Error Propagation Functionality.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""

import pytest
from pytest_bdd import parsers, scenario, when
from ska_tango_base.commands import ResultCode

from tests.settings import ERROR_PROPAGATION_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp-72336.feature",
    "MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for EndScan "
    + "command",
)
def test_endscan_error_propagation(
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test case to verify the Error propagation functionality for
    EndScan command on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke EndScan command on a defective MCCS Subarray"))
def endscan_command_mccs_sln(mccssln_device, mccs_subarray):
    """Invokes EndScan command on MCCS Subarray Leaf Node."""
    try:
        mccs_subarray.SetDefective(ERROR_PROPAGATION_DEFECT)
        pytest.command_result = mccssln_device.EndScan()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking EndScan on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="EndScan command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
