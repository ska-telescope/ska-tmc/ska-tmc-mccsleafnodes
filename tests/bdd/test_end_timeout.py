"""
BDD Test cases for End Command for Timeout Functionality.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""

import json

import pytest
from pytest_bdd import parsers, scenario, when
from ska_control_model import ObsState
from ska_tango_base.commands import ResultCode

from tests.settings import STUCK_IN_INTERMEDIATE_STATE_DEFECT, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28154.feature",
    "MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for End "
    + "command",
)
def test_end_timeout(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the Timeout functionality for End command on
    MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke End command on a defective MCCS Subarray"))
def end_mccs_sln(mccssln_device, mccs_subarray):
    """Invokes End command on MCCS Subarray Leaf Node."""
    try:
        DEFECT = json.loads(STUCK_IN_INTERMEDIATE_STATE_DEFECT)
        DEFECT["intermediate_state"] = ObsState.SCANNING
        mccs_subarray.SetDefective(json.dumps(DEFECT))
        pytest.command_result = mccssln_device.End()
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking End on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="End command failed on the MCCS Subarray Leaf Node."
        )


# @then -> ../conftest.py
