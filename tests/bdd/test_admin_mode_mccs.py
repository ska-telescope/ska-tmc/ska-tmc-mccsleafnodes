import time

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from ska_control_model import AdminMode
from ska_tango_base.control_model import ObsState

from tests.settings import (
    RESULTCODE_OK,
    logger,
    tear_down_mccsmln,
    wait_for_attribute_value,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/test_admin_mode.feature",
    "Test admin mode functionality MCCS subarray",
)
def test_admin_mode_verification() -> None:
    """
    Test case to verify admin mode functionality
    """


@given("the MCCS Subarray is in adminMode ONLINE set by leafnode")
def set_mccs_subarray_admin_mode(
    mccssln_device,
    mccs_subarray,
    mccsmln_device,
    mccs_controller,
    group_callback,
) -> None:
    """Set the adminMode of MCCS subarray to ONLINE"""
    try:
        mccssln_device.setadminmode(AdminMode.ONLINE)
        mccsmln_device.setadminmode(AdminMode.ONLINE)
        assert mccs_subarray.adminMode == AdminMode.ONLINE
        assert mccs_controller.adminMode == AdminMode.ONLINE
        mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        _, unique_id = mccsmln_device.On()
        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=10,
        )
    except Exception as e:
        logger.info("not able to set the adminMode %s", e)


@given("MCCS Subarray is in IDLE obsState after AssignResources command")
def invoke_assign_resources_command(
    json_factory, group_callback, mccsmln_device, mccssln_device
):
    """Ensure MCCSSubarrayLeafNode is in obsState IDLE"""

    assign_json_str = json_factory("command_AssignResources")
    _, unique_id = mccsmln_device.AssignResources(assign_json_str)
    mccsmln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK),
        lookahead=10,
    )
    group_callback["obsState"].assert_change_event(
        ObsState.IDLE,
        lookahead=12,
    )


@when("adminMode of MCCS Subarray is set to OFFLINE by leafnode")
def set_admin_mode_to_offline(
    mccssln_device, mccs_subarray, mccsmln_device, mccs_controller
):
    """set the admin mode to offline"""
    try:
        mccssln_device.setadminmode(AdminMode.OFFLINE)
        mccsmln_device.setadminmode(AdminMode.OFFLINE)
        assert mccs_subarray.adminMode == AdminMode.OFFLINE
        assert mccs_controller.adminMode == AdminMode.OFFLINE
    except Exception as e:
        logger.info("not able to set the adminMode %s", e)


@then("MCCS Subarray denies invocation of releaseresources command")
def execute_release_resources(
    group_callback, mccsmln_device, json_factory
) -> None:
    """Execute release resources on MCCSSubarrayLeafNode"""
    release_input_str = json_factory("command_Release")
    pytest.command_result = mccsmln_device.ReleaseAllResources(
        release_input_str
    )
    time.sleep(40)
    exception = "Command Release not allowed"
    assert exception in mccsmln_device.longRunningCommandResult[1]
    mccsmln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )


@then("MCCS Subarray remains in IDLE obsState")
def check_mccs_subarray_obsstate(
    group_callback, mccssln_device, mccs_subarray, mccsmln_device, json_factory
):
    """check the MCCS subarray obsstate"""
    wait_for_attribute_value(mccs_subarray, "obsState", "IDLE")
    assert mccs_subarray.obsstate == ObsState.IDLE
    # tear down
    mccsmln_device.setadminmode(AdminMode.ONLINE)
    mccssln_device.setadminmode(AdminMode.ONLINE)
    release_input_str = json_factory("command_Release")
    _, unique_id = mccsmln_device.ReleaseAllResources(release_input_str)
    mccsmln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK),
        lookahead=10,
    )
    group_callback["obsState"].assert_change_event(
        ObsState.EMPTY,
        lookahead=12,
    )

    tear_down_mccsmln(mccsmln_device, group_callback)
