"""
BDD Test cases for Configure Command.
This module contains BDD tests for the LowTmcLeafNodeMccsSubarray.
"""
import pytest
import tango
from pytest_bdd import parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import RESULTCODE_OK, logger, tear_down_mccsmln


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28149.feature",
    "MCCS Subarray Leaf Node executes Configure successfully",
)
def test_configure_command(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the Configure functionality on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(
    parsers.parse("I invoke Configure command on the MCCS Subarray Leaf Node")
)
def configure_mccs_sln(mccssln_device, json_factory):
    """Invokes Configure command on MCCS Subarray Leaf Node."""
    try:
        configure_input_json = json_factory("mccssln_configure_json")
        pytest.command_result = mccssln_device.Configure(configure_input_json)
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Configure on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Configure command failed on the MCCS Subarray Leaf Node."
        )


@then(parsers.parse("the ObsState changes from IDLE to READY"))
def verify_configure_completion(
    mccsmln_device, mccssln_device, group_callback
):
    """Verifies that the MCCS Subarray ObsState successfully transitioned to
    READY ObsState.
    """
    unique_id = pytest.command_result[1][0]

    mccssln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )

    group_callback["obsState"].assert_change_event(
        ObsState.CONFIGURING,
        lookahead=12,
    )
    group_callback["obsState"].assert_change_event(
        ObsState.READY,
        lookahead=12,
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, RESULTCODE_OK),
        lookahead=12,
    )
    tear_down_mccsmln(mccsmln_device, group_callback)
