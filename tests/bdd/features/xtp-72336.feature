@XTP-72336
Scenario: MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for EndScan command
    Given an MCCS Subarray in the SCANNING obsState
	When I invoke EndScan command on a defective MCCS Subarray
	Then Exception Occurs on MCCS Subarry leaf node and gets propagated