@XTP-28245
Scenario:  MCCS Subarray Leaf Node executes Scan successfully
  Given an MCCS Subarray in the READY obsState
  When I invoke Scan command on the MCCS Subarray Leaf Node
  Then the ObsState changes from READY to SCANNING
