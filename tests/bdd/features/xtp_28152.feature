@XTP-28152
Scenario: MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for Configure command
    Given an MCCS Subarray in the IDLE obsState
	When I invoke Configure command on a defective MCCS Subarray
	Then the command times out, reporting the timeout error.
