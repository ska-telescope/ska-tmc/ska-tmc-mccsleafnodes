@XTP-28154
Scenario: MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for End command
    Given an MCCS Subarray in the READY obsState
	When I invoke End command on a defective MCCS Subarray
	Then the command times out, reporting the timeout error.
