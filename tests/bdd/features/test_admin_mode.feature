# This BDD test verifies the admin mode functionality
Scenario Outline: Test admin mode functionality MCCS subarray
    Given the MCCS Subarray is in adminMode ONLINE set by leafnode
    And MCCS Subarray is in IDLE obsState after AssignResources command
    When adminMode of MCCS Subarray is set to OFFLINE by leafnode
    Then MCCS Subarray denies invocation of releaseresources command
    And MCCS Subarray remains in IDLE obsState