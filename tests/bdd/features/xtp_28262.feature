@XTP-28262
Scenario: MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for EndScan command
    Given an MCCS subarray in the SCANNING obsState
	When I invoke EndScan command on a defective MCCS Subarray
	Then the command times out, reporting the timeout error.
