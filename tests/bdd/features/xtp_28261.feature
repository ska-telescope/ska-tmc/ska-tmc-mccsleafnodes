@XTP-28261
Scenario: MCCS Subarray Leaf Node: Timeout error from MCCS Subarray for Scan command
    Given an MCCS Subarray in the READY obsState
	When I invoke Scan command on a defective MCCS Subarray
	Then the command times out, reporting the timeout error.
