@XTP-28310
Scenario: MCCS Subarray Leaf Node executes an Abort command in EMPTY obsState
  Given an MCCS Subarray in the EMPTY obsState
  When I invoke Abort command on the MCCS Subarray Leaf Node
  Then MCCS Subarray Leaf Node raises command not allowed exception
  Then the MCCS Subarray device remains in the obsState EMPTY
