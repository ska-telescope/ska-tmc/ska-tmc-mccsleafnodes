@XTP-72319
Scenario: MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for End command
    Given an MCCS Subarray in the READY obsState
	When I invoke END command on a defective MCCS Subarray
	Then Exception Occurs on MCCS Subarry leaf node and gets propagated