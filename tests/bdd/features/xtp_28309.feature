@XTP-28309
Scenario:  MCCS Subarray Leaf Node executes Abort Restart successfully
  Given an MCCS Subarray in the <obsState> obsState
  When I invoke Abort command on the MCCS Subarray Leaf Node
  Then the MCCS Subarray obsState changes to ABORTED
  Then I invoke Restart command on the MCCS Subarray Leaf Node
  Then the Mccs Subarray changes obsState to the EMPTY

  Examples:
  | obsState        |
  | RESOURCING      |
  | IDLE            |
  | CONFIGURING     |
  | READY           |
  | SCANNING        |
