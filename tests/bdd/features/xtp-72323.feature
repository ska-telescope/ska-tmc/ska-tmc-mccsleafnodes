@XTP-72323
Scenario: MCCS Subarray Leaf Node: Error Propagation for MCCS Subarray for Scan command
    Given an MCCS Subarray in the READY obsState
	When I invoke Scan command on a defective MCCS Subarray
	Then Exception Occurs on MCCS Subarry leaf node and gets propagated