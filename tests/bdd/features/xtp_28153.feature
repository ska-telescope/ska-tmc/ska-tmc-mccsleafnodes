@XTP-28153
Scenario:  MCCS Subarray Leaf Node executes End successfully
  Given an MCCS Subarray in the READY obsState
  When I invoke End command on the MCCS Subarray Leaf Node
  Then the ObsState changes from READY to IDLE
