@XTP-28256
Scenario:  MCCS Subarray Leaf Node executes EndScan successfully
   Given an MCCS subarray in the SCANNING obsState
   When I invoke EndScan command on MCCS Subarray Leaf Node
   Then the ObsState changes from SCANNING to READY
