@XTP-28149
Scenario:  MCCS Subarray Leaf Node executes Configure successfully
  Given an MCCS Subarray in the IDLE obsState
  When I invoke Configure command on the MCCS Subarray Leaf Node
  Then the ObsState changes from IDLE to READY
