"""
BDD test cases of Scan command.

This module contains bdd test scenarios for the LowTmcLeafNodeMccsSubarray.
"""
import pytest
import tango
from pytest_bdd import parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import RESULTCODE_OK, logger, tear_down_mccsmln


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28245.feature",
    "MCCS Subarray Leaf Node executes Scan successfully",
)
def test_scan_command(set_mccs_device_admin_mode_for_integration_tests):
    """
    Test case to verify the Scan functionality on MCCS Subarray Leaf Node.
    """


# @given -> ../conftest.py


@when(parsers.parse("I invoke Scan command on the MCCS Subarray Leaf Node"))
def scan_mccs_sln(mccssln_device, json_factory):
    """Invokes Scan command on MCCS Subarray Leaf Node."""
    try:
        scan_input_str = json_factory("mccssln_scan_json")
        pytest.command_result = mccssln_device.Scan(scan_input_str)
        assert pytest.command_result[0][0] == ResultCode.QUEUED
    except Exception as exception:
        logger.exception(
            "Exception occured while invoking Scan on MCCS SLN: %s",
            exception,
        )
        pytest.fail(
            reason="Scan command failed on the MCCS Subarray Leaf Node."
        )


@then(parsers.parse("the ObsState changes from READY to SCANNING"))
def verify_scan_completion(mccsmln_device, mccssln_device, group_callback):
    """Verifies that the MCCS Subarray ObsState successfully transitioned to
    SCANNING ObsState.
    """
    unique_id = pytest.command_result[1][0]

    mccssln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    mccssln_device.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )

    group_callback["obsState"].assert_change_event(
        ObsState.SCANNING,
        lookahead=8,
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, RESULTCODE_OK),
        lookahead=8,
    )
    tear_down_mccsmln(mccsmln_device, group_callback)
