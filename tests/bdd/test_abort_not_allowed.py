import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.settings import RESULTCODE_OK, logger


@pytest.mark.acceptance
@pytest.mark.SKA_low
@scenario(
    "../bdd/features/xtp_28310.feature",
    "MCCS Subarray Leaf Node executes an Abort command in EMPTY obsState",
)
def test_abort_command_not_allowed_empty(
    set_mccs_device_admin_mode_for_integration_tests,
):
    """
    Test case to verify the abort command not allowed in EMPTY obsState.
    """


@given(parsers.parse("an MCCS Subarray in the EMPTY obsState"))
def given_mccs(group_callback, mccs_subarray, mccsmln_device):
    """Given an MCCS Subarray Leaf Node in a particular obsState"""
    try:
        LRCR_ID_MLN = mccsmln_device.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            group_callback["longRunningCommandResult"],
        )
        # Invoke On command on MCCS Master Leaf Node
        result, unique_id = mccsmln_device.On()
        assert result[0] == ResultCode.QUEUED

        group_callback["longRunningCommandResult"].assert_change_event(
            (unique_id[0], RESULTCODE_OK),
            lookahead=8,
        )
        OBS_STATE_ID = mccs_subarray.subscribe_event(
            "obsState",
            tango.EventType.CHANGE_EVENT,
            group_callback["obsState"],
        )
        group_callback["obsState"].assert_change_event(
            ObsState.EMPTY,
            lookahead=8,
        )
        mccsmln_device.unsubscribe_event(LRCR_ID_MLN)
        mccs_subarray.unsubscribe_event(OBS_STATE_ID)
    except Exception as exception:
        logger.exception(
            "Exception occurred while invoking Assign SLN: %s",
            exception,
        )
        pytest.fail(reason=str(exception))


@when(parsers.parse("I invoke Abort command on the MCCS Subarray Leaf Node"))
def abort_mccs_sln(mccssln_device):
    """Invoke the Abort command on MCCS Subarray Leaf Node."""
    try:
        mccssln_device.Abort()
    except tango.DevFailed as ex:
        pytest.command_result = str(ex)


@then(
    parsers.parse(
        "MCCS Subarray Leaf Node raises command not allowed exception"
    )
)
def abort_exception():
    """Assert the exception message"""
    assert (
        "CommandNotAllowed: Abort command on this device is not allowed."
        in pytest.command_result
    )


@then(parsers.parse("the MCCS Subarray device remains in the obsState EMPTY"))
def abort_not_completed(
    group_callback,
    mccs_subarray,
    mccsmln_device,
):
    """Verify that the obsState remains EMPTY"""
    OBS_STATE_ID = mccs_subarray.subscribe_event(
        "obsState",
        tango.EventType.CHANGE_EVENT,
        group_callback["obsState"],
    )
    group_callback["obsState"].assert_change_event(
        ObsState.EMPTY,
        lookahead=8,
    )

    result, unique_id = mccsmln_device.Off()
    assert result[0] == ResultCode.QUEUED
    LRCR_ID_MLN = mccsmln_device.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], RESULTCODE_OK),
        lookahead=8,
    )
    mccsmln_device.unsubscribe_event(LRCR_ID_MLN)
    mccs_subarray.unsubscribe_event(OBS_STATE_ID)
